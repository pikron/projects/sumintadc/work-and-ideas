% Component values commputation

% Single supply version

% Input impedance
Ri=33e3;

% Cycle period
Tmod=5e-6; %200kHz

% Input voltage range
Vinmax=1;
Vinmin=-1;

% Refference voltage
Vref=2.5;

% Maximum output voltage swing
Vo_max=1;


D0=Vinmax/(Vinmax-Vinmin)*0.4+0.05;

R1=R2*(1/D0-1)
R2=Ri
R3=R1
R4=Ri
V0=Vref*R2/(R1+R2);
R5=V0/((Vref-V0)/R3-(V0-Vinmax*1.25)/R4)

IC1_Pmin=(Vref-V0)/R3+(Vref-V0)/R5-(V0-Vinmin)/R4;
C1=Tmod*0.45*IC1_Pmin/Vo_max

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Symmetrical supply version

% Input impedance
Ri=47e3;

% Cycle period
Tmod=100e-6; %10kHz

% Input voltage range
Vinmax=1;
Vinmin=-1;

% Positive refference voltage
Vrp=2.5;

% Maximum output voltage swing
Vo_max=1.5;


D0=Vinmax/(Vinmax-Vinmin)*0.4+0.05;

Vrn=-Vrp/(1/D0-1)

R6=Ri
R7=-Vrn/(Vinmax*1.25/R6)
IC1_Pmin=Vrp/R7+Vinmin/R6;
C1=Tmod*0.45*IC1_Pmin/Vo_max
