#!/usr/bin/python
# -*- coding: utf-8 -*-

# lcd5000_cdf.py -s -L 'avg 1x,avg 2x,avg 4x,avg 8x,avg 16x,avg 32x,avg 64x' lcd5000-25hz-aver40ms.dat lcd5000-25hz-aver80ms.dat lcd5000-25hz-aver160ms.dat lcd5000-25hz-aver320ms.dat lcd5000-25hz-aver640ms.dat lcd5000-25hz-aver1280ms.dat lcd5000-25hz-aver2560ms.dat 

import os
import csv
import re
import sys
import argparse
import time

import numpy as np

from matplotlib import pyplot as plt
from datetime import datetime as dt

def str_or_empty(o):
    if o is None:
        return ''
    else:
        return str(o)

class lcd_cdf_raw_data:
    def __init__(self):
        self.time = None
        self.reference = None
        self.input_signal = None
        self.adc_data = None
    def load_dat(self, fname):
        self.adc_data = np.loadtxt(fname_dat, dtype = np.double, ndmin = 2)

class lcd_cdf_measuremet:
    def __init__(self, fname_dat = None):
        self.data = lcd_cdf_raw_data()
        self.data_min = None
        self.data_max = None
        self.data_aver = None
        self.data_std = None
        self.bin_step = None
        self.bin_step_count = None
        self.bin_min = None
        self.bin_max = None
        self.bin_range = None
        self.bin_count = None
        if fname_dat is not None:
            self.data.load_dat(fname_dat)

    def process(self, sub_trend_order = None):
        adc_data = self.data.adc_data
        if sub_trend_order is not None:
            x = np.arange(len(adc_data), dtype = np.double)
            pf = np.polyfit(x, adc_data, sub_trend_order)
            adc_data -= np.expand_dims(np.polyval(pf, x), axis = 1)
        self.data_min = np.min(adc_data)
        self.data_max = np.max(adc_data)
        self.data_aver = np.average(adc_data)
        self.data_std = np.std(adc_data)
        d = self.data_max - self.data_min
        r = 1e8
        while 1 / r * 1e5 < d:
            r /= 10
        self.bin_step = 1 / r
        print(self.bin_step)
        bmin = np.longlong(np.floor(self.data_min * r) - 1)
        self.bin_min = bmin / r
        bmax = np.longlong(np.ceil(self.data_max * r) + 1)
        self.bin_max = bmax / r
        self.bin_step_count = bmax - bmin + 1
        print('bins count = {:}, step = {:}, min = {:}, max = {:}'.format(self.bin_step_count, self.bin_step, self.bin_min, self.bin_max))
        self.bin_range = np.double((np.add(bmin, range(0, self.bin_step_count)))) / r
        b = np.asarray(np.longlong(np.round(np.multiply(adc_data, r)) - bmin), dtype = int)
        #print(adc_data)
        #print(b)
        unique, counts = np.unique(b, return_counts=True)
        self.bin_count = np.zeros(self.bin_step_count, dtype = int)
        self.bin_count[unique] = counts
        self.bin_cdf = np.cumsum(self.bin_count)

    def plot(self, ax_cdf = None, ax_data = None, sub_aver = False, plot_label = None):

        if plot_label is None:
            plot_label = 'CDF'

        ax = ax_cdf
        if ax is None:
            fig = plt.figure()
            ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
            ax.set_title('LCD5000 SumInt CDF')
            ax.set_xlabel('value')
            ax.set_ylabel('CDF [# samples]')

        x = self.bin_range
        if sub_aver:
            x = np.subtract(x, self.data_aver)
        ax.plot(x, self.bin_cdf, label=plot_label)

        ax = ax_data
        if ax is None:
            fig = plt.figure()
            ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
            ax.set_title('LCD5000 SumInt data')
            ax.set_xlabel('time [s]')
            ax.set_ylabel('value')

        y = self.data.adc_data
        if sub_aver:
            y = np.subtract(y, self.data_aver)
        x = np.divide(range(0, len(y)), 25.0)
        ax.plot(x, y, label=plot_label)


if __name__ == '__main__':
    help_msg = '''SYNOPSIS: lcd5000_cdf.py'''

    parser = argparse.ArgumentParser(description='LCD500 SumInt ADC data processing')
    parser.add_argument('-l', '--label', dest='label', type=str,
                        default=None, help='additional string to the graphs title')
    parser.add_argument('-L', '--curve-labels', dest='clabels', type=str,
                        default=None, help='curve labels')
    parser.add_argument('-s', '--subaver', dest='sub_aver', action='store_true',
                      default=False, help='substract average from measurement')
    parser.add_argument('-t', '--subtrend', dest='sub_trend_order', type=int,
                      default=None, help='substract trend with specified order from measurement')
    parser.add_argument('data_files', help='ADC output data', nargs=argparse.REMAINDER)

    args = parser.parse_args()

    #print(args.nom_range_min, args.nom_range_max)

    measurements = []

    for fname_dat in args.data_files:
        meas = lcd_cdf_measuremet(fname_dat = fname_dat)
        measurements.append(meas)
        meas.process(sub_trend_order = args.sub_trend_order)

    if len(measurements) < 1:
        print('No measurement parsed')
        exit(1)

    title_label = ''
    if args.label is not None:
        title_label += ' ' + args.label

    curve_labels = []
    if args.clabels is not None:
        curve_labels = args.clabels.split(',')

    fig = plt.figure()
    ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    ax.set_title('LCD5000 SumInt CDF')
    ax.set_xlabel('value')
    ax.set_ylabel('CDF [# samples]')
    ax_cdf = ax

    fig = plt.figure()
    ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    ax.set_title('LCD5000 SumInt data')
    ax.set_xlabel('time [s]')
    ax.set_ylabel('value')
    ax_data = ax

    fig = plt.figure()
    ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    ax.set_title('LCD5000 SumInt standard deviation')
    ax.set_xlabel('sample')
    ax.set_ylabel('value')
    ax_std = ax

    idx = 0
    curves_std = []
    for meas in measurements:
        clabel = None
        if idx < len(curve_labels):
            clabel = curve_labels[idx]
        meas.plot(ax_cdf = ax_cdf, ax_data = ax_data, sub_aver = args.sub_aver, plot_label = clabel)
        curves_std.append(meas.data_std)
        idx += 1

    ax_cdf.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    ax_data.legend(loc='center left', bbox_to_anchor=(1, 0.5))


    ax_std.plot(range(1, len(curves_std) + 1), curves_std)

    plt.show()
