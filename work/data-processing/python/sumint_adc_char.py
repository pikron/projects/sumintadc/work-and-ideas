#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import csv
import re
import sys
import argparse
import time

import numpy as np

from matplotlib import pyplot as plt
from datetime import datetime as dt

from sumint_adc_model_single_supply import sumint_adc_model_single_supply

def str_or_empty(o):
    if o is None:
        return ''
    else:
        return str(o)

class adc_measuremet_samples:
    def __init__(self):
        self.time = None
        self.reference = None
        self.input_signal = None
        self.adc_avg = None
        self.adc_std = None
        self.adc_min = None
        self.adc_max = None
        self.adc_blind = None

class adc_measuremet_params:
    def __init__(self):
        self.head_fname = None
        self.csv_fname = None
        self.raw_dir = None
        self.nom_range_min = None
        self.nom_range_max = None
        self.voltage_start = None
        self.voltage_stop = None
        self.voltage_incr = None
        self.temperature = None
        self.humidity = None
        self.channel = None
        self.blind_channel = None
        self.stage_count = None
        self.stage_points = None
        self.stage_delay = None

    def fill_from_file(self, head_fname):
        self.head_fname = head_fname
        with open(self.head_fname, 'rt', encoding="utf8", errors='ignore') as head_file:
            for line in head_file:
                fields = line.split(':', maxsplit = 1)
                if len(fields) >= 2:
                    vals = fields[1].split()
                if fields[0] == 'temperature':
                    self.temperature = np.double(vals[0])
                elif fields[0] == 'humidity':
                    self.humidity = np.double(vals[0])
                elif fields[0] == 'channel':
                    self.channel = np.int(vals[0])
                elif fields[0] == 'blind channel':
                    self.blind_channel = np.int(vals[0])
                elif fields[0] == 'start voltage':
                    self.voltage_start = np.double(vals[0])
                elif fields[0] == 'stop voltage':
                    self.voltage_stop = np.double(vals[0])
                elif fields[0] == 'voltage increment':
                    self.voltage_incr = np.double(vals[0])
                elif fields[0] == 'delay between stages':
                    self.stage_delay = np.double(vals[0])
                elif fields[0] == 'number of stages':
                    self.stage_count = np.int(vals[0])
                elif fields[0] == 'number of points in one stage':
                    self.stage_points = np.int(vals[0])
                elif fields[0] == 'data file name':
                    if self.csv_fname is None:
                        csv_fname = vals[0].replace('\\', '/')
                        csv_fname_local = os.path.basename(csv_fname)
                        csv_fname_local = os.path.join(os.path.dirname(self.head_fname),csv_fname_local)
                        if os.path.exists(csv_fname_local):
                            csv_fname = csv_fname_local
                    self.csv_fname = csv_fname
        if self.raw_dir is None and self.csv_fname is not None:
            mo = re.fullmatch('.*_([0-9]+)\.[^.]*', self.csv_fname)
            if mo is not None:
                raw_subdir = 'raw_data_' + mo.group(1)
                self.raw_dir = os.path.join(os.path.dirname(self.csv_fname), raw_subdir)

class adc_measuremet:
    def __init__(self, head_fname=None, csv_fname=None, raw_dir=None,
                 nom_range_min=None, nom_range_max = None, subtract_blind = None,
                 raw_channel = -1, minmax_compute = False, adjust_for_subsamples = False,
                 mod_averaging_period = None):
        self.params = adc_measuremet_params()
        self.params.head_fname = head_fname
        self.params.csv_fname = csv_fname
        self.params.raw_dir = raw_dir
        self.params.nom_range_min = nom_range_min
        self.params.nom_range_max = nom_range_max
        self.pf_params = None
        self.smaples_in_range = None
        self.inl_abs_max = None
        self.inl_range_single_sample = None
        self.std_max = None
        self.subtract_blind = False

        if subtract_blind is not None:
            self.subtract_blind = subtract_blind

        if self.params.head_fname is not None:
            self.params.fill_from_file(self.params.head_fname)

        if self.params.voltage_start is not None and self.params.voltage_stop is not None:
            if self.params.nom_range_min is None:
                self.params.nom_range_min = min(self.params.voltage_start, self.params.voltage_stop)
            if self.params.nom_range_max is None:
                self.params.nom_range_max = max(self.params.voltage_start, self.params.voltage_stop)

        if mod_averaging_period is None:
            mod_averaging_period = 50000

        self.samples = adc_measuremet_samples()

        print(vars(self.params))

        with open(self.params.csv_fname, 'rt') as csv_file:
            csv_reader = csv.reader(csv_file, dialect='excel', delimiter=',')
            csv_id = next(csv_reader)
            columns = []
            for id in csv_id:
                columns.append([])
            #print (csv_id)
            #csv_map = [x for x in csv_fields]
            #i for i, s in enumerate(mylist) if 'aa' in s
            #print csv_id
            #print ','.join(csv_fields)
            #print csv_fields
            #print csv_map
            for row in csv_reader:
                for i in range(len(csv_id)):
                    if csv_id[i] == 'time':
                        columns[i].append(dt.fromisoformat(row[i]))
                    else: 
                        columns[i].append(np.double(row[i]))
            for i in range(len(csv_id)):
                if csv_id[i] == 'time':
                    self.samples.time = columns[i]
                elif csv_id[i] == 'reference':
                    self.samples.reference = np.asarray(columns[i], dtype = np.double)
                elif csv_id[i] == 'input_signal':
                    self.samples.input_signal = np.asarray(columns[i], dtype = np.double)
                elif csv_id[i] == 'adc_avg' and raw_channel == -1:
                    self.samples.adc_avg = np.asarray(columns[i], dtype = np.double)
                elif csv_id[i] == 'adc_std' and raw_channel == -1:
                    self.samples.adc_std = np.asarray(columns[i], dtype = np.double)
                elif csv_id[i] == 'adc_blind':
                    self.samples.adc_blind = np.asarray(columns[i], dtype = np.double)
            #print(columns)
        if raw_channel >= 0:
            self.params.channel = raw_channel
            adc_avg = np.empty(np.shape(self.samples.input_signal), dtype=np.double)
            adc_std = np.empty(np.shape(self.samples.input_signal), dtype=np.double)
            adc_min = np.empty(np.shape(self.samples.input_signal), dtype=np.double)
            adc_max = np.empty(np.shape(self.samples.input_signal), dtype=np.double)
            first = True
            for i in range(len(self.samples.input_signal)):
                raw_fname = os.path.join(self.params.raw_dir, 'all_raw_data_' + str(i) + '.txt')
                c = 0
                with open(raw_fname, 'r') as f:
                    cmd_reply = f.readline()
                    for line in f.readlines():
                        value_sample = np.asarray(line.split("\t"), dtype=np.double)
                        value_sample = np.reshape(value_sample, (1, len(value_sample)))
                        if first:
                           first = False
                           value_series = value_sample
                        else:
                           if c >= np.shape(value_series)[0]:
                               value_series = np.concatenate((value_series, value_series), axis = 0)
                           value_series[c] = value_sample
                        c += 1
                valraw = value_series[range(c), raw_channel * 2]
                valss = value_series[range(c), raw_channel * 2 + 1]
                if adjust_for_subsamples:
                    sample_time = mod_averaging_period + valss[1:c] - valss[0:c-1]
                    val = np.multiply(np.divide(valraw[1:c], sample_time), mod_averaging_period)
                else:
                    val = valraw
                adc_avg[i] = np.mean(valraw)
                adc_std[i] = np.std(val)
                adc_min[i] = np.min(val)
                adc_max[i] = np.max(val)
            self.samples.adc_avg = adc_avg
            self.samples.adc_std = adc_std
            self.samples.adc_min = adc_min
            self.samples.adc_max = adc_max
        else:
            if minmax_compute or adjust_for_subsamples:
                adc_min = np.empty(np.shape(self.samples.input_signal), dtype=np.double)
                adc_max = np.empty(np.shape(self.samples.input_signal), dtype=np.double)
                for i in range(len(self.samples.input_signal)):
                    raw_csv_fname = os.path.join(self.params.raw_dir, 'raw_data_' + str(i) + '.csv')
                    with open(raw_csv_fname, 'r') as f:
                        csv_reader = csv.reader(f, dialect='excel', delimiter=',')
                        csv_id = next(csv_reader)
                        columns = []
                        for id in csv_id:
                             columns.append([])
                        for row in csv_reader:
                             for c in range(len(csv_id)):
                                 columns[c].append(np.double(row[c]))
                        for c in range(len(csv_id)):
                             if csv_id[c] == 'values':
                                 valraw = np.asarray(columns[c], dtype = np.double)
                             if csv_id[c] == 'subsample_values':
                                 valss = np.asarray(columns[c], dtype = np.double)
                        c = len(valraw)
                        if adjust_for_subsamples:
                            sample_time = mod_averaging_period + valss[1:c] - valss[0:c-1]
                            val = np.multiply(np.divide(valraw[1:c], sample_time), mod_averaging_period)
                        else:
                            val = valraw
                        adc_min[i] = np.min(val)
                        adc_max[i] = np.max(val)
                self.samples.adc_min = adc_min
                self.samples.adc_max = adc_max
        #print(vars(self.samples))

    def compute_inl_range_single_sample(self, pf_params = None):
        pf = pf_params
        if pf is None:
            pf = self.pf_params

        idx = self.smaples_in_range
        x = self.samples.input_signal[idx]
        y_min = self.samples.adc_min[idx]
        y_max = self.samples.adc_max[idx]
        if self.subtract_blind:
            y_min -= self.samples.adc_blind[idx]
            y_max -= self.samples.adc_blind[idx]
        y_min_err = y_min - pf[1] - x * pf[0]
        y_max_err = y_max - pf[1] - x * pf[0]
        y_err_min = min(np.min(y_min_err),np.min(y_max_err))
        y_err_max = max(np.max(y_min_err),np.max(y_max_err))
        return (y_err_min, y_err_max)

    def process(self):
        idx=np.where(np.logical_and(
                (self.samples.input_signal >= self.params.nom_range_min - 0.0004),
                (self.samples.input_signal <= self.params.nom_range_max + 0.004)))
        #x = np.take(self.samples.input_signal, idx, 0)[0]
        #y = np.take(self.samples.adc_avg, idx, 0)[0]

        x = self.samples.input_signal[idx]
        if self.subtract_blind:
            y = self.samples.adc_avg[idx] - self.samples.adc_blind[idx]
        else:
            y = self.samples.adc_avg[idx]

        #print(np.shape(self.samples.input_signal))
        #print(np.shape(x))

        pf = np.polyfit(x, y, 1)

        self.smaples_in_range = idx
        self.pf_params = pf

        self.inl_abs_max = np.max(np.abs(y - pf[1] - x * pf[0]))

        if (self.samples.adc_min is not None) and (self.samples.adc_max is not None):
            self.inl_range_single_sample = self.compute_inl_range_single_sample()

        self.std_max = np.max(self.samples.adc_std[self.smaples_in_range])

    def plot(self, pf_params = None, ax_nom_err = None, ax_full_err = None,
             ax_conversion = None, range_relative = False, channel_to_label = False,
             plot_min_max_range = False):

        pf = pf_params
        if pf is None:
            pf = self.pf_params

        idx = self.smaples_in_range

        x = self.samples.input_signal[idx]
        if self.subtract_blind:
            y = self.samples.adc_avg[idx] - self.samples.adc_blind[idx]
        else:
            y = self.samples.adc_avg[idx]

        nom_adc_err = y - pf[1] - x * pf[0]

        if self.subtract_blind:
            full_y = self.samples.adc_avg - self.samples.adc_blind
        else:
            full_y = self.samples.adc_avg

        full_adc_err = full_y - pf[1] - self.samples.input_signal * pf[0]

        if range_relative:
            y_label_err = "of range"
            nom_range_volts = self.params.nom_range_max - self.params.nom_range_min
            nom_range_levels = nom_range_volts * pf[0]
            nom_adc_err /= nom_range_levels
            full_adc_err /= nom_range_levels
        else:
            y_label_err = "n [LSB]"

        plot_label = '?'
        if self.params.temperature is not None:
            plot_label = '{:} °C'.format(self.params.temperature)
        if channel_to_label:
            plot_label += ' ch' + str(self.params.channel)

        ax = ax_nom_err
        if ax is None:
            fig = plt.figure()
            ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
            ax.set_title('ADC error nominal range')
            ax.set_xlabel("U [V]")
            ax.set_ylabel(y_label_err)
        ax.plot(x, nom_adc_err, label=plot_label)

        if plot_min_max_range:
            y_min = self.samples.adc_min[idx]
            y_max = self.samples.adc_max[idx]
            if self.subtract_blind:
                y_min -= self.samples.adc_blind[idx]
                y_max -= self.samples.adc_blind[idx]
            y_min_err = y_min - pf[1] - x * pf[0]
            y_max_err = y_max - pf[1] - x * pf[0]
            if range_relative:
                y_min_err /= nom_range_levels
                y_max_err /= nom_range_levels
            ax.plot(x, y_min_err)
            ax.plot(x, y_max_err)

        ax = ax_full_err
        if ax is None:
            fig = plt.figure()
            ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
            ax.set_title('ADC error full range')
            ax.set_xlabel("U [V]")
            ax.set_ylabel(y_label_err)
        ax.plot(self.samples.input_signal, full_adc_err, label=plot_label)

        ax = ax_conversion
        if ax is None:
            fig = plt.figure()
            ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
            ax.set_title('ADC conversion')
            ax.set_xlabel("U [V]")
            ax.set_ylabel("n [LSB]")
        ax.plot(self.samples.input_signal, full_y, 'b', label=plot_label)
        ax.plot(x, pf[1] + x * pf[0], 'y')

if __name__ == '__main__':
    help_msg = '''SYNOPSIS: sumint_adrc_char.py'''

    parser = argparse.ArgumentParser(description='SumInt ADC characterization data processing')
    parser.add_argument('-m', '--nom-range-min', dest='nom_range_min', type=np.double,
                        default=-1.0, help='nominal range minimal voltage')
    parser.add_argument('-M', '--nom-range-max', dest='nom_range_max', type=np.double,
                        default=+1.0, help='nominal range maximal voltage')
    parser.add_argument('-t', '--nom-temperature', dest='nom_temperature', type=np.double,
                        default=25.0, help='nominal temperature')
    parser.add_argument('-l', '--label', dest='label', type=str,
                        default=None, help='additional string to the graphs title')
    parser.add_argument('-b', '--subtract-blind', dest='subtract_blind', action='store_true',
                      default=False, help='subtract blind channel before evaluation')
    parser.add_argument('-c', '--raw-channels', dest='raw_channels', type=str,
                        default=None, help='extract channel(s) data from the raw data')
    parser.add_argument('-r', '--relative', dest='range_relative', action='store_true',
                      default=False, help='compute the error relative to the nominal input range')
    parser.add_argument('-s', '--summary', dest='summary_fname', type=str,
                        default=None, help='export summary data')
    parser.add_argument('-a', '--adjust-for-subsamples', dest='adjust_for_subsamples', action='store_true',
                      default=False, help='adjust maxima-minima computation for subsamples')
    parser.add_argument('-P', '--mod-averaging-period', dest='mod_averaging_period', type=int,
                        default=None, help='specify averaging period in modulator clocks cycles')
    parser.add_argument('adc_measurement', help='measurement header files', nargs=argparse.REMAINDER)

    args = parser.parse_args()

    #print(args.nom_range_min, args.nom_range_max)

    if args.raw_channels is None:
        channels = [-1]
    else:
        channels = list(map(int, args.raw_channels.split(',')))

    adc_measurements = []

    if args.summary_fname is None:
        minmax_compute = False
    else:
        minmax_compute = True

    for adc_measurement_fname in args.adc_measurement:
        for ch in channels:
            meas = adc_measuremet(adc_measurement_fname,
                              nom_range_min = args.nom_range_min,
                              nom_range_max = args.nom_range_max,
                              subtract_blind = args.subtract_blind,
                              raw_channel = ch,
                              minmax_compute = minmax_compute,
                              adjust_for_subsamples = args.adjust_for_subsamples,
                              mod_averaging_period = args.mod_averaging_period)
            adc_measurements.append(meas)
            meas.process()

    if len(adc_measurements) < 1:
        print('No measurement parsed')
        exit(1)

    channels = []
    for meas in adc_measurements:
        if meas.params.channel is not None:
           if not meas.params.channel in channels:
               channels.append(meas.params.channel)

    if args.range_relative:
        y_label_err = "of range"
    else:
        y_label_err = "n [LSB]"

    title_label = ''
    if args.label is not None:
        title_label += ' ' + args.label

    fig = plt.figure()
    ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    ax.set_title('SumInt ADC error nominal range - channel' + str(channels) + title_label)
    ax.set_xlabel("U [V]")
    ax.set_ylabel(y_label_err)
    ax_nom_err = ax

    fig = plt.figure()
    ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    ax.set_title('ADC error full range - channel' + str(channels) + title_label)
    ax.set_xlabel("U [V]")
    ax.set_ylabel(y_label_err)
    ax_full_err = ax

    fig = plt.figure()
    ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    ax.set_title('ADC conversion - channel' + str(channels) + title_label)
    ax.set_xlabel("U [V]")
    ax.set_ylabel("n [LSB]")
    ax_conversion = ax


    pf_params = {}
    if adc_measurements[0].params.channel is None:
        ch = -1
    else:
        ch = adc_measurements[0].params.channel
    pf_params[ch] = adc_measurements[0].pf_params
    temp_diff = {}
    inl_abs_max_for_nom_temperature = {}
    nom_temp = args.nom_temperature
    reference_t_nom = {}
    meas_t_nom = {}

    for meas in adc_measurements:
        if meas.params.temperature is not None:
            if meas.params.channel is None:
                ch = -1
            else:
                ch = meas.params.channel
            temp_diff_this = np.abs(meas.params.temperature - nom_temp)
            if ch in temp_diff:
                if temp_diff_this > temp_diff[ch]:
                    continue
                if temp_diff_this == temp_diff[ch] and meas.inl_abs_max >= inl_abs_max_for_nom_temperature[ch]:
                    continue
            meas_t_nom[ch] = meas
            temp_diff[ch] = temp_diff_this
            pf_params[ch] = meas.pf_params
            inl_abs_max_for_nom_temperature[ch] = meas.inl_abs_max
            if meas.samples.reference is None:
                continue
            reference_t_nom[ch] = np.average(meas.samples.reference)

    for meas in adc_measurements:
        if meas.samples.reference is not None:
            print(np.average(meas.samples.reference))

    for meas in adc_measurements:
        if meas.params.channel is None:
            ch = -1
        else:
            ch = meas.params.channel
        meas.plot(pf_params = pf_params[ch], ax_nom_err = ax_nom_err,
                  ax_full_err = ax_full_err, ax_conversion = ax_conversion,
                  range_relative = args.range_relative,
                  channel_to_label = (len(channels) > 1))

    ax_nom_err.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    ax_full_err.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    ax_conversion.legend(loc='center left', bbox_to_anchor=(1, 0.5))

    mdl = sumint_adc_model_single_supply()

    fig = plt.figure()
    ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    ax.set_title('ADC error by model - calibrated at nominal vref - actual vref - channel' + str(channels) + title_label)
    ax.set_xlabel("U [V]")
    ax.set_ylabel("U [V]")
    ax_model_err = ax

    meas = meas_t_nom[next(iter(meas_t_nom))]
    ref = reference_t_nom[next(iter(meas_t_nom))]
    mdl.update_model(Vref = ref)
    pf = mdl.get_tranformation_cycles_to_voltage()
    adc_voltages = meas.samples.adc_avg * pf[0] + pf[1]

    idx=np.where(np.logical_and(
                (meas.samples.input_signal >= meas.params.nom_range_min - 0.0004),
                (meas.samples.input_signal <= meas.params.nom_range_max + 0.004)))

    x = meas.samples.input_signal[idx]
    y = adc_voltages[idx]

    pf_calib_t_nom = np.polyfit(x, y, 1)

    print(pf_calib_t_nom)

    for meas in adc_measurements:
        if meas.samples.reference is not None:
            Vref = np.average(meas.samples.reference)
            mdl.update_model(Vref = Vref)
            pf = mdl.get_tranformation_cycles_to_voltage()
            adc_voltages = meas.samples.adc_avg * pf[0] + pf[1]
            adc_voltages = (adc_voltages - pf_calib_t_nom[1]) / pf_calib_t_nom[0]
            adc_voltages_err = adc_voltages - meas.samples.input_signal
            plot_label = '?'
            if meas.params.temperature is not None:
                plot_label = '{:} °C'.format(meas.params.temperature)
            ax = ax_model_err
            ax.plot(meas.samples.input_signal, adc_voltages_err, label=plot_label)
            #ax.plot(meas.samples.input_signal, adc_voltages, label=plot_label)
            print(min(adc_voltages_err), max(adc_voltages_err))

    ax_model_err.legend(loc='center left', bbox_to_anchor=(1, 0.5))

    if args.summary_fname is not None:
        with open(args.summary_fname,'wt') as sumcsvfile:
            sumcsvfile.write('temp,err_at_t,std_max,err_min_at_t,err_max_at_t,err_min_abs,err_max_abs\n')
            first = True
            for meas in adc_measurements:
                if (meas.samples.adc_min is None) and (meas.samples.adc_max is None):
                    continue
                if meas.params.channel is None:
                    ch = -1
                else:
                    ch = meas.params.channel
                ia_max = meas.inl_abs_max
                irss_min = meas.inl_range_single_sample[0]
                irss_max = meas.inl_range_single_sample[1]
                irss_min_ot, irss_max_ot = meas.compute_inl_range_single_sample(pf_params = pf_params[ch])
                std_max = meas.std_max
                if args.range_relative:
                    nom_range_volts = meas.params.nom_range_max - meas.params.nom_range_min
                    nom_range_levels = nom_range_volts * meas.pf_params[0]
                    print('nom_range_volts ' + str(nom_range_volts) + ' nom_range_levels ' + str(nom_range_levels))
                    ia_max /= nom_range_levels
                    irss_min /= nom_range_levels
                    irss_max /= nom_range_levels
                    irss_min_ot /= nom_range_levels
                    irss_max_ot /= nom_range_levels
                    std_max /= np.abs(nom_range_levels)
                ia_max = np.abs(ia_max)
                irss_min, irss_max = (min(irss_min, irss_max), max(irss_min, irss_max))
                irss_min_ot, irss_max_ot = (min(irss_min_ot, irss_max_ot), max(irss_min_ot, irss_max_ot))
                print('temp ' + str(meas.params.temperature) +
                      ' inl ' + str(ia_max) + ' range ' + str(irss_min) + ' ' + str(irss_max) +
                      ' over temp ' + str(irss_min_ot) + ' ' + str(irss_max_ot))
                sumcsvfile.write(str(meas.params.temperature) + ',' +
                                 str(ia_max) + ',' +
                                 str(std_max) + ',' +
                                 str(irss_min) + ',' +
                                 str(irss_max) + ',' +
                                 str(irss_min_ot) + ',' +
                                 str(irss_max_ot) + '\n')
                if first:
                    ia_max_total = ia_max
                    irss_min_total = irss_min
                    irss_max_total = irss_max
                    irss_min_ot_total = irss_min_ot
                    irss_max_ot_total = irss_max_ot
                    std_max_total = std_max
                    first = False
                else:
                    ia_max_total = max(ia_max_total, ia_max)
                    irss_min_total = min(irss_min_total, irss_min)
                    irss_max_total = max(irss_max_total, irss_max)
                    irss_min_ot_total = min(irss_min_ot_total, irss_min_ot)
                    irss_max_ot_total = max(irss_max_ot_total, irss_max_ot)
                    std_max_total = max(std_max_total,std_max)

            print('total single sample error for single run' + str(irss_min_total) + ' ' + str(irss_max_total))
            print('total single sample error over temperature ' + str(irss_min_ot_total) + ' ' + str(irss_max_ot_total))
            sumcsvfile.write('total,' +
                             str(ia_max_total) + ',' +
                             str(std_max_total) + ',' +
                             str(irss_min_total) + ',' +
                             str(irss_max_total) + ',' +
                             str(irss_min_ot_total) + ',' +
                             str(irss_max_ot_total) + '\n')

    plt.show()
