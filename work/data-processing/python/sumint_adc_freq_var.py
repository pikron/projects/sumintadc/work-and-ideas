#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import csv
import re
import sys
import argparse
import time

import numpy as np

from matplotlib import pyplot as plt
from datetime import datetime as dt

class captured_samples:
    def __init__(self, samples_fname, sample_rate = 200000):
        self.sample_rate = sample_rate
        self.samples_fname = samples_fname
        self.samples = None
        self.last_subsamples = None

        with open(samples_fname, 'rt') as csv_file:
            csv_reader = csv.reader(csv_file, dialect='excel', delimiter=',')
            csv_id = next(csv_reader)
            columns = []
            for id in csv_id:
                columns.append([])
            for row in csv_reader:
                for i in range(len(csv_id)):
                    if csv_id[i] == 'time':
                        columns[i].append(dt.fromisoformat(row[i]))
                    else:
                        columns[i].append(np.double(row[i]))
            for i in range(len(csv_id)):
                if csv_id[i] == 'values':
                    self.samples = np.asarray(columns[i], dtype = np.double)
                elif csv_id[i] == 'subsample_values':
                    self.last_subsamples = np.asarray(columns[i], dtype = np.double)

        #self.samples = np.array([100, 100, 100+1, 100-1, 100, 100, 100, 100, 100, 100], dtype = np.double)

    def process(self, remove_trend = False, distinct_windows = False):
        self.samples_mean = np.mean(self.samples)
        if remove_trend:
            x = np.arange(0, np.size(self.samples, 0), 1)
            pf = np.polyfit(x, self.samples, 1)
            self.samples_err = self.samples - pf[1] - x * pf[0]
        else:
            self.samples_err = self.samples - np.mean(self.samples)
        self.samples_err_cumsum = np.cumsum(self.samples_err)

        self.num_decim_points = np.int(np.size(self.samples_err, 0) / 2)
        self.decims = np.arange(1, self.num_decim_points + 1, 1, dtype=np.int)
        self.freqs = np.divide(self.sample_rate, self.decims)
        self.variances = np.ndarray(self.num_decim_points, dtype=np.double)

        for i in range(self.num_decim_points):
            d = self.decims[i]
            c = np.size(self.samples_err_cumsum, 0)
            if distinct_windows:
                idx = np.arange(0, c - d, d, dtype = np.int)
            else:
                idx = np.arange(0, c - d, 1, dtype = np.int)
            self.variances[i] = np.sqrt(np.mean(np.power(np.divide(self.samples_err_cumsum[idx + d] - self.samples_err_cumsum[idx], d), 2)))

    def plot(self,  ax_freq_var = None, plot_label = 'SumInt ADC variance vs. sampling frequency'):
        ax = ax_freq_var
        if ax is None:
            fig = plt.figure()
            ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
            ax.set_title('ADC error nominal range')
            ax.set_xlabel("sampling frequency [Hz]")
            ax.set_ylabel("RMS error [LSB]")
            ax.set_yscale('log')
            ax.set_xscale('log')
        ax.plot(self.freqs, self.variances, label=plot_label)


if __name__ == '__main__':
    help_msg = '''SYNOPSIS: sumint_adrc_char.py'''

    parser = argparse.ArgumentParser(description='SumInt ADC variance over frequencies')
    parser.add_argument('captured_serie', help='CSV file with samples data', nargs=argparse.REMAINDER)

    args = parser.parse_args()


    fig = plt.figure()
    ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    ax.set_title('SumInt ADC variance over frequencies (1 LSB .. 1/500 of full range)')
    ax.set_xlabel("sampling frequency [Hz]")
    ax.set_ylabel("RMS error [LSB]")
    ax.set_yscale('log')
    ax.set_xscale('log')
    ax_freq_var = ax


    series = []

    for serie_fname in args.captured_serie:
        serie = captured_samples(serie_fname)
        series.append(serie)
        serie.process()
        serie.plot(ax_freq_var = ax_freq_var, plot_label = 'moving')
        serie.process(distinct_windows = True)
        serie.plot(ax_freq_var = ax_freq_var, plot_label = 'distinct')

    ax_freq_var.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.show()
