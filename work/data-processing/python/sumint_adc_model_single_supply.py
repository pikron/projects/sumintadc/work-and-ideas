#!/usr/bin/python
# -*- coding: utf-8 -*-

# Single supply SumInt ADC version

class sumint_adc_model_single_supply:
    def __init__(self):
        # componenets values
        self.R1 = 100e3
        self.R2 = 33e3

        self.R3 = 100e3
        self.R4 = 33e3
        self.R5 = 15e3

        self.C1 = 470e-12

        # Cycle period
        self.fmod = 200e3   # 200kHz
        self.Tmod = 1/self.fmod

        # ADC fast clock
        self.fadc = 200e6   # 200MHz
        self.Tadc = 1/self.fadc

        # Averaging frequency, sample rate
        self.faver = 4e3     # 4 kHz
        self.Taver = 1/self.faver

        # Input voltage range
        self.Vinmax = 1.0
        self.Vinmin = -1.0

        # Refference voltage
        self.Vref = 2.5

        self.update_model()

    def update_model(self, Vref = None):
        if Vref is not None:
            self.Vref = Vref

        self.V0 = self.Vref*self.R2/(self.R1+self.R2)

        self.IC1_P0 = (self.Vref-self.V0)/self.R3+(self.Vref-self.V0)/self.R5-self.V0/self.R4
        self.IC1_N0 = (self.Vref-self.V0)/self.R3-self.V0/self.R5-self.V0/self.R4

        self.t1 = self.Tmod*-self.IC1_N0/(self.IC1_P0-self.IC1_N0)

        # Optimal duty cycle range is 0.05 to 0.45
        # Optimal output voltage swing depend on opamp supply,
        #   opamp slew rate and comparator noise

        # Duty cycle for zerro input voltage
        self.D_0 = self.t1/self.Tmod
        # Output voltage swing for zerro input voltage
        self.Uo_0 = self.t1*self.IC1_P0/self.C1

        self.IC1_Pmax = (self.Vref-self.V0)/self.R3+(self.Vref-self.V0)/self.R5-(self.V0-self.Vinmax)/self.R4
        self.IC1_Nmax = (self.Vref-self.V0)/self.R3-self.V0/self.R5-(self.V0-self.Vinmax)/self.R4

        self.t1max = self.Tmod*-self.IC1_Nmax/(self.IC1_Pmax-self.IC1_Nmax)
        # Duty cycle for maximal input voltage
        self.D_max = self.t1max/self.Tmod
        # Output voltage swing for maximal input voltage
        self.Uo_max = self.t1max*self.IC1_Pmax/self.C1

        self.IC1_Pmin = (self.Vref-self.V0)/self.R3+(self.Vref-self.V0)/self.R5-(self.V0-self.Vinmin)/self.R4;
        self.IC1_Nmin = (self.Vref-self.V0)/self.R3-self.V0/self.R5-(self.V0-self.Vinmin)/self.R4;

        self.t1min = self.Tmod*-self.IC1_Nmin/(self.IC1_Pmin-self.IC1_Nmin);
        # Duty cycle for maximal input voltage
        self.D_min = self.t1min/self.Tmod
        # Output voltage swing for minimal input voltage
        self.Uo_min = self.t1min*self.IC1_Pmin/self.C1

    def get_tranformation_cycles_to_voltage(self):
        # Decimation given by output averaging
        decim = self.Taver / self.Tmod
        # Measured cycles for zero input voltage
        cycles_0 = self.D_0 * self.Tmod / self.Tadc * decim
        volts_per_cycle = (self.Vinmin - self.Vinmax) / ((self.t1min - self.t1max) / self.Tadc) / decim
        return [volts_per_cycle, -volts_per_cycle * cycles_0]
