EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 16
Title "SumIntADC NX breadboard"
Date "2021-11-12"
Rev "1.0"
Comp "PiKRON"
Comment1 "Pavel Píša, Petr Porazil, Jakub Ladman"
Comment2 "Self-test Signal Generator"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ics:DAC9881SBRGET U?
U 1 1 631B09B7
P 4750 3150
AR Path="/631B09B7" Ref="U?"  Part="1" 
AR Path="/61CEB696/631B09B7" Ref="U?"  Part="1" 
AR Path="/622F77ED/631B09B7" Ref="U?"  Part="1" 
AR Path="/622F77ED/6310EED9/631B09B7" Ref="U901"  Part="1" 
F 0 "U901" H 5450 2850 50  0000 L CNN
F 1 "DAC9881SBRGET" V 5700 2550 50  0000 L CNN
F 2 "dac_ti:QFN50P400X400X100-25N-D" H 6200 3650 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/dac9881" H 6200 3550 50  0001 L CNN
F 4 "18-bit, single-channel, low-noise, voltage output DAC for high accuracy applications" H 6200 3450 50  0001 L CNN "Description"
F 5 "1" H 6200 3350 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 6200 3250 50  0001 L CNN "Manufacturer_Name"
F 7 "DAC9881SBRGET" H 6200 3150 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "595-DAC9881SBRGET" H 6200 3050 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/DAC9881SBRGET?qs=nDUVEfKy%252B0axdDdcVhQPCA%3D%3D" H 6200 2950 50  0001 L CNN "Mouser Price/Stock"
F 10 "DAC9881SBRGET" H 6200 2850 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/dac9881sbrget/texas-instruments" H 6200 2750 50  0001 L CNN "Arrow Price/Stock"
	1    4750 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 3150 4450 3150
Text HLabel 4450 3150 0    50   Input ~ 0
SCLK
Wire Wire Line
	4750 3250 4450 3250
Text HLabel 4450 3250 0    50   Input ~ 0
MOSI
Wire Wire Line
	4650 3650 4750 3650
Wire Wire Line
	5650 4450 5650 4650
Wire Wire Line
	5650 4650 5250 4650
Wire Wire Line
	5250 4650 5250 4450
Wire Wire Line
	5350 4450 5450 4450
Wire Wire Line
	5350 4450 5350 4750
Wire Wire Line
	5350 4750 4450 4750
Connection ~ 5350 4450
Text HLabel 4450 4750 0    50   Output ~ 0
V_OUT
Wire Wire Line
	6350 3150 6450 3150
Wire Wire Line
	6350 3450 6450 3450
Connection ~ 6450 3450
Wire Wire Line
	6450 3450 6450 3550
Wire Wire Line
	6350 3550 6450 3550
Connection ~ 6450 3550
Wire Wire Line
	5650 2450 5650 2150
Connection ~ 5650 2150
Wire Wire Line
	5350 2450 5350 2150
Connection ~ 5350 2150
Wire Wire Line
	5350 2150 5450 2150
NoConn ~ 5450 2450
NoConn ~ 6350 3650
NoConn ~ 5750 4450
Connection ~ 5550 4850
Wire Wire Line
	4750 3450 4650 3450
Wire Wire Line
	4650 3450 4650 3650
Wire Wire Line
	4750 3550 4550 3550
Wire Wire Line
	4750 3350 4650 3350
Wire Wire Line
	4650 3350 4650 3450
Connection ~ 4650 3450
Wire Wire Line
	6350 3250 6550 3250
Text Notes 6650 3550 0    50   ~ 0
USB - unsigned\n after Power On Reset the \noutput starts at GNDDAC ( = -VHR = -2.07V versus GNDHR))
Text Notes 6650 3250 0    50   ~ 0
RST - Inactive, relying on internal POR
Wire Wire Line
	5550 4450 5550 4850
Wire Wire Line
	5550 2450 5550 2350
Wire Wire Line
	5550 2350 5250 2350
Wire Wire Line
	5250 2450 5250 2350
Connection ~ 5250 2350
Wire Wire Line
	5250 2350 5150 2350
Connection ~ 5750 2150
Wire Wire Line
	5650 2150 5750 2150
Wire Wire Line
	5750 2150 5750 2450
Text HLabel 4450 1850 0    50   Input ~ 0
~CS
Wire Wire Line
	5850 2450 5850 1850
Wire Wire Line
	5850 1850 5750 1850
$Comp
L Device:R_Small R?
U 1 1 631B0A09
P 5750 2000
AR Path="/61833839/631B0A09" Ref="R?"  Part="1" 
AR Path="/622F77ED/631B0A09" Ref="R?"  Part="1" 
AR Path="/622F77ED/6310EED9/631B0A09" Ref="R905"  Part="1" 
F 0 "R905" H 5600 1950 50  0000 R CNN
F 1 "10k" H 5600 2050 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5750 2000 50  0001 C CNN
F 3 "~" H 5750 2000 50  0001 C CNN
	1    5750 2000
	-1   0    0    1   
$EndComp
Wire Wire Line
	5750 2100 5750 2150
Wire Wire Line
	5750 1900 5750 1850
Connection ~ 5750 1850
Wire Wire Line
	5750 1850 4450 1850
$Comp
L Device:C_Small C?
U 1 1 631B0A13
P 5550 2250
AR Path="/631B0A13" Ref="C?"  Part="1" 
AR Path="/622F77ED/631B0A13" Ref="C?"  Part="1" 
AR Path="/622F77ED/6310EED9/631B0A13" Ref="C902"  Part="1" 
F 0 "C902" V 5650 2250 50  0000 C CNN
F 1 "100n" V 5550 1650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5550 2250 50  0001 C CNN
F 3 "https://cz.mouser.com/datasheet/2/396/mlcc02_e-1307760.pdf" H 5550 2250 50  0001 C CNN
	1    5550 2250
	-1   0    0    1   
$EndComp
Connection ~ 5550 2350
Connection ~ 5550 2150
Wire Wire Line
	5550 2150 5650 2150
$Comp
L Device:C_Small C?
U 1 1 631B0A1C
P 5250 2250
AR Path="/631B0A1C" Ref="C?"  Part="1" 
AR Path="/622F77ED/631B0A1C" Ref="C?"  Part="1" 
AR Path="/622F77ED/6310EED9/631B0A1C" Ref="C901"  Part="1" 
F 0 "C901" V 5350 2250 50  0000 C CNN
F 1 "10u/16" V 5250 1650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5250 2250 50  0001 C CNN
F 3 "https://cz.mouser.com/datasheet/2/396/mlcc02_e-1307760.pdf" H 5250 2250 50  0001 C CNN
	1    5250 2250
	-1   0    0    1   
$EndComp
Wire Wire Line
	5250 2150 5350 2150
$Comp
L Transistor_FET:BSS123 Q?
U 1 1 631B0A36
P 5050 5850
AR Path="/622F77ED/631B0A36" Ref="Q?"  Part="1" 
AR Path="/622F77ED/61ACF1DD/631B0A36" Ref="Q?"  Part="1" 
AR Path="/622F77ED/6310EED9/631B0A36" Ref="Q901"  Part="1" 
F 0 "Q901" H 5200 6000 50  0000 L CNN
F 1 "BSS205N" H 4800 5650 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5250 5775 50  0001 L CIN
F 3 "https://www.infineon.com/cms/en/product/power/mosfet/small-signal-small-power/bss205n/#!?fileId=db3a304330f686060131091244950062" H 5050 5850 50  0001 L CNN
	1    5050 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 5850 4750 5850
Wire Wire Line
	5150 6050 5150 6350
Wire Wire Line
	4750 6150 4750 6350
$Comp
L Device:R_Small R?
U 1 1 631B0A53
P 4750 6050
AR Path="/61833839/631B0A53" Ref="R?"  Part="1" 
AR Path="/622F77ED/631B0A53" Ref="R?"  Part="1" 
AR Path="/622F77ED/6310EED9/631B0A53" Ref="R901"  Part="1" 
F 0 "R901" H 4700 5950 50  0000 R CNN
F 1 "220k" H 4700 6150 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4750 6050 50  0001 C CNN
F 3 "~" H 4750 6050 50  0001 C CNN
	1    4750 6050
	-1   0    0    1   
$EndComp
Wire Wire Line
	4750 5950 4750 5850
Connection ~ 4750 5850
Text HLabel 4500 5850 0    50   Input ~ 0
POL
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J?
U 1 1 631B0A60
P 6800 6000
AR Path="/622F77ED/631B0A60" Ref="J?"  Part="1" 
AR Path="/622F77ED/6310EED9/631B0A60" Ref="J901"  Part="1" 
F 0 "J901" H 6850 6317 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 6850 6226 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x04_P2.54mm_Vertical" H 6800 6000 50  0001 C CNN
F 3 "~" H 6800 6000 50  0001 C CNN
	1    6800 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 5900 6600 6000
Connection ~ 6600 6000
Wire Wire Line
	6600 6000 6600 6100
Connection ~ 6600 6100
Wire Wire Line
	6600 6100 6600 6200
Wire Wire Line
	7100 6200 7100 6100
Connection ~ 7100 6000
Wire Wire Line
	7100 6000 7100 5900
Connection ~ 7100 6100
Wire Wire Line
	7100 6100 7100 6000
Wire Wire Line
	7350 5900 7100 5900
Connection ~ 7100 5900
$Comp
L Device:C_Small C?
U 1 1 61877833
P 5650 4750
AR Path="/61877833" Ref="C?"  Part="1" 
AR Path="/622F77ED/61877833" Ref="C?"  Part="1" 
AR Path="/622F77ED/6310EED9/61877833" Ref="C903"  Part="1" 
F 0 "C903" H 5550 4850 50  0000 R CNN
F 1 "10u/16" H 5550 4950 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5650 4750 50  0001 C CNN
F 3 "https://cz.mouser.com/datasheet/2/396/mlcc02_e-1307760.pdf" H 5650 4750 50  0001 C CNN
	1    5650 4750
	-1   0    0    1   
$EndComp
Wire Wire Line
	5650 4850 5550 4850
Connection ~ 5650 4650
$Comp
L project_power:GNDDAC #PWR0908
U 1 1 618D2051
P 5550 4850
F 0 "#PWR0908" H 5550 4600 50  0001 C CNN
F 1 "GNDDAC" H 5555 4677 50  0000 C CNN
F 2 "" H 5550 4850 50  0001 C CNN
F 3 "" H 5550 4850 50  0001 C CNN
	1    5550 4850
	1    0    0    -1  
$EndComp
$Comp
L project_power:GNDDAC #PWR0913
U 1 1 618D252D
P 6450 3550
F 0 "#PWR0913" H 6450 3300 50  0001 C CNN
F 1 "GNDDAC" H 6455 3377 50  0000 C CNN
F 2 "" H 6450 3550 50  0001 C CNN
F 3 "" H 6450 3550 50  0001 C CNN
	1    6450 3550
	1    0    0    -1  
$EndComp
$Comp
L project_power:GNDDAC #PWR0903
U 1 1 618D2865
P 4650 3650
F 0 "#PWR0903" H 4650 3400 50  0001 C CNN
F 1 "GNDDAC" H 4655 3477 50  0000 C CNN
F 2 "" H 4650 3650 50  0001 C CNN
F 3 "" H 4650 3650 50  0001 C CNN
	1    4650 3650
	1    0    0    -1  
$EndComp
Connection ~ 4650 3650
$Comp
L project_power:GNDDAC #PWR0905
U 1 1 618D2BBD
P 5150 2350
F 0 "#PWR0905" H 5150 2100 50  0001 C CNN
F 1 "GNDDAC" H 4900 2300 50  0000 C CNN
F 2 "" H 5150 2350 50  0001 C CNN
F 3 "" H 5150 2350 50  0001 C CNN
	1    5150 2350
	1    0    0    -1  
$EndComp
$Comp
L project_power:VDDDAC #PWR0907
U 1 1 618D34A5
P 5450 2150
F 0 "#PWR0907" H 5450 2000 50  0001 C CNN
F 1 "VDDDAC" H 5450 2300 50  0000 C CNN
F 2 "" H 5450 2150 50  0001 C CNN
F 3 "" H 5450 2150 50  0001 C CNN
	1    5450 2150
	1    0    0    -1  
$EndComp
Connection ~ 5450 2150
Wire Wire Line
	5450 2150 5550 2150
$Comp
L project_power:VDDDAC #PWR0914
U 1 1 618D55D0
P 6550 3250
F 0 "#PWR0914" H 6550 3100 50  0001 C CNN
F 1 "VDDDAC" H 6565 3423 50  0000 C CNN
F 2 "" H 6550 3250 50  0001 C CNN
F 3 "" H 6550 3250 50  0001 C CNN
	1    6550 3250
	1    0    0    -1  
$EndComp
$Comp
L project_power:VDDDAC #PWR0912
U 1 1 618D5A1C
P 6300 4650
F 0 "#PWR0912" H 6300 4500 50  0001 C CNN
F 1 "VDDDAC" H 6315 4823 50  0000 C CNN
F 2 "" H 6300 4650 50  0001 C CNN
F 3 "" H 6300 4650 50  0001 C CNN
	1    6300 4650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6300 4650 6100 4650
$Comp
L project_power:VDDDAC #PWR0902
U 1 1 618D7DF4
P 4550 3550
F 0 "#PWR0902" H 4550 3400 50  0001 C CNN
F 1 "VDDDAC" H 4350 3600 50  0000 C CNN
F 2 "" H 4550 3550 50  0001 C CNN
F 3 "" H 4550 3550 50  0001 C CNN
	1    4550 3550
	1    0    0    -1  
$EndComp
$Comp
L project_power:GNDHR #PWR0906
U 1 1 618DB7CE
P 5150 6350
F 0 "#PWR0906" H 5150 6100 50  0001 C CNN
F 1 "GNDHR" H 5155 6177 50  0000 C CNN
F 2 "" H 5150 6350 50  0001 C CNN
F 3 "" H 5150 6350 50  0001 C CNN
	1    5150 6350
	1    0    0    -1  
$EndComp
$Comp
L project_power:GNDHR #PWR0915
U 1 1 618DBBEB
P 7350 5900
F 0 "#PWR0915" H 7350 5650 50  0001 C CNN
F 1 "GNDHR" H 7355 5727 50  0000 C CNN
F 2 "" H 7350 5900 50  0001 C CNN
F 3 "" H 7350 5900 50  0001 C CNN
	1    7350 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 61C58FDA
P 5950 4650
AR Path="/61833839/61C58FDA" Ref="R?"  Part="1" 
AR Path="/622F77ED/61C58FDA" Ref="R?"  Part="1" 
AR Path="/622F77ED/6310EED9/61C58FDA" Ref="R906"  Part="1" 
F 0 "R906" V 6054 4650 50  0000 C CNN
F 1 "1k" V 6150 4700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5950 4650 50  0001 C CNN
F 3 "~" H 5950 4650 50  0001 C CNN
	1    5950 4650
	0    1    1    0   
$EndComp
Wire Wire Line
	5850 4650 5800 4650
$Comp
L Device:D_Small D901
U 1 1 61C6A159
P 5950 4550
F 0 "D901" H 5950 4757 50  0000 C CNN
F 1 "1N4148W" H 5950 4666 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" V 5950 4550 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85748/1n4148w.pdf" V 5950 4550 50  0001 C CNN
	1    5950 4550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6050 4550 6100 4550
Wire Wire Line
	6100 4550 6100 4650
Connection ~ 6100 4650
Wire Wire Line
	6100 4650 6050 4650
Wire Wire Line
	5850 4550 5800 4550
Wire Wire Line
	5800 4550 5800 4650
Connection ~ 5800 4650
Wire Wire Line
	5800 4650 5650 4650
$Comp
L Amplifier_Operational:MCP6001-OT U?
U 1 1 617C1EDC
P 5650 5900
AR Path="/617C1EDC" Ref="U?"  Part="1" 
AR Path="/617B514C/617C1EDC" Ref="U?"  Part="1" 
AR Path="/619065BF/617C1EDC" Ref="U?"  Part="1" 
AR Path="/61908BA4/617C1EDC" Ref="U?"  Part="1" 
AR Path="/6192AB24/617C1EDC" Ref="U?"  Part="1" 
AR Path="/619332FF/617C1EDC" Ref="U?"  Part="1" 
AR Path="/619B691C/617C1EDC" Ref="U?"  Part="1" 
AR Path="/619BF925/617C1EDC" Ref="U?"  Part="1" 
AR Path="/619C8940/617C1EDC" Ref="U?"  Part="1" 
AR Path="/619BE637/617C1EDC" Ref="U?"  Part="1" 
AR Path="/619D45F6/617C1EDC" Ref="U?"  Part="1" 
AR Path="/619EA585/617C1EDC" Ref="U?"  Part="1" 
AR Path="/622F77ED/6310EED9/617C1EDC" Ref="U902"  Part="1" 
F 0 "U902" H 5250 5900 50  0000 L CNN
F 1 "ADA4084-1ARJZ-R2" H 5850 5450 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5550 5700 50  0001 L CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADA4084-1_4084-2_4084-4.pdf" H 5650 6100 50  0001 C CNN
	1    5650 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 617C1EE2
P 5650 5600
AR Path="/617C1EE2" Ref="C?"  Part="1" 
AR Path="/617B514C/617C1EE2" Ref="C?"  Part="1" 
AR Path="/619065BF/617C1EE2" Ref="C?"  Part="1" 
AR Path="/61908BA4/617C1EE2" Ref="C?"  Part="1" 
AR Path="/6192AB24/617C1EE2" Ref="C?"  Part="1" 
AR Path="/619332FF/617C1EE2" Ref="C?"  Part="1" 
AR Path="/619B691C/617C1EE2" Ref="C?"  Part="1" 
AR Path="/619BF925/617C1EE2" Ref="C?"  Part="1" 
AR Path="/619C8940/617C1EE2" Ref="C?"  Part="1" 
AR Path="/619BE637/617C1EE2" Ref="C?"  Part="1" 
AR Path="/619D45F6/617C1EE2" Ref="C?"  Part="1" 
AR Path="/619EA585/617C1EE2" Ref="C?"  Part="1" 
AR Path="/622F77ED/6310EED9/617C1EE2" Ref="C906"  Part="1" 
F 0 "C906" V 5650 5400 50  0000 C CNN
F 1 "100p" V 5650 5900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5650 5600 50  0001 C CNN
F 3 "~" H 5650 5600 50  0001 C CNN
	1    5650 5600
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 617C1EE8
P 5650 5300
AR Path="/617C1EE8" Ref="C?"  Part="1" 
AR Path="/617B514C/617C1EE8" Ref="C?"  Part="1" 
AR Path="/619065BF/617C1EE8" Ref="C?"  Part="1" 
AR Path="/61908BA4/617C1EE8" Ref="C?"  Part="1" 
AR Path="/6192AB24/617C1EE8" Ref="C?"  Part="1" 
AR Path="/619332FF/617C1EE8" Ref="C?"  Part="1" 
AR Path="/619B691C/617C1EE8" Ref="C?"  Part="1" 
AR Path="/619BF925/617C1EE8" Ref="C?"  Part="1" 
AR Path="/619C8940/617C1EE8" Ref="C?"  Part="1" 
AR Path="/619BE637/617C1EE8" Ref="C?"  Part="1" 
AR Path="/619D45F6/617C1EE8" Ref="C?"  Part="1" 
AR Path="/619EA585/617C1EE8" Ref="C?"  Part="1" 
AR Path="/622F77ED/6310EED9/617C1EE8" Ref="C904"  Part="1" 
F 0 "C904" V 5650 5100 50  0000 C CNN
F 1 "100n" V 5650 5600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5650 5300 50  0001 C CNN
F 3 "~" H 5650 5300 50  0001 C CNN
	1    5650 5300
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 617C1EEE
P 5650 5450
AR Path="/617C1EEE" Ref="C?"  Part="1" 
AR Path="/617B514C/617C1EEE" Ref="C?"  Part="1" 
AR Path="/619065BF/617C1EEE" Ref="C?"  Part="1" 
AR Path="/61908BA4/617C1EEE" Ref="C?"  Part="1" 
AR Path="/6192AB24/617C1EEE" Ref="C?"  Part="1" 
AR Path="/619332FF/617C1EEE" Ref="C?"  Part="1" 
AR Path="/619B691C/617C1EEE" Ref="C?"  Part="1" 
AR Path="/619BF925/617C1EEE" Ref="C?"  Part="1" 
AR Path="/619C8940/617C1EEE" Ref="C?"  Part="1" 
AR Path="/619BE637/617C1EEE" Ref="C?"  Part="1" 
AR Path="/619D45F6/617C1EEE" Ref="C?"  Part="1" 
AR Path="/619EA585/617C1EEE" Ref="C?"  Part="1" 
AR Path="/622F77ED/6310EED9/617C1EEE" Ref="C905"  Part="1" 
F 0 "C905" V 5650 5250 50  0000 C CNN
F 1 "10n" V 5650 5750 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5650 5450 50  0001 C CNN
F 3 "~" H 5650 5450 50  0001 C CNN
	1    5650 5450
	0    1    1    0   
$EndComp
Wire Wire Line
	5550 5600 5550 5450
Connection ~ 5550 5600
Connection ~ 5550 5450
Wire Wire Line
	5550 5450 5550 5300
Wire Wire Line
	5750 5300 5750 5450
Connection ~ 5750 5450
Wire Wire Line
	5750 5450 5750 5600
$Comp
L project_power:GNDHR #PWR0911
U 1 1 617C3FC5
P 5750 5600
F 0 "#PWR0911" H 5750 5350 50  0001 C CNN
F 1 "GNDHR" H 5850 5450 50  0000 C CNN
F 2 "" H 5750 5600 50  0001 C CNN
F 3 "" H 5750 5600 50  0001 C CNN
	1    5750 5600
	1    0    0    -1  
$EndComp
Connection ~ 5750 5600
$Comp
L project_power:+5VHR #PWR0909
U 1 1 617C43F1
P 5550 5300
F 0 "#PWR0909" H 5550 5150 50  0001 C CNN
F 1 "+5VHR" H 5565 5473 50  0000 C CNN
F 2 "" H 5550 5300 50  0001 C CNN
F 3 "" H 5550 5300 50  0001 C CNN
	1    5550 5300
	1    0    0    -1  
$EndComp
Connection ~ 5550 5300
Connection ~ 6600 5900
$Comp
L project_power:+VREF #PWR0901
U 1 1 61B6E303
P 4650 5550
F 0 "#PWR0901" H 4650 5400 50  0001 C CNN
F 1 "+VREF" H 4665 5723 50  0000 C CNN
F 2 "" H 4650 5550 50  0001 C CNN
F 3 "" H 4650 5550 50  0001 C CNN
	1    4650 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 5900 6600 5900
Wire Wire Line
	4650 5550 4650 6250
Connection ~ 4650 5550
Wire Wire Line
	4650 6250 4850 6250
Wire Wire Line
	4850 5550 4650 5550
Wire Wire Line
	5350 5550 5150 5550
Wire Wire Line
	5350 5800 5350 5550
Wire Wire Line
	5350 6250 5600 6250
Connection ~ 5350 6250
Wire Wire Line
	5350 6000 5350 6250
Wire Wire Line
	5800 6250 5950 6250
Wire Wire Line
	5050 6250 5350 6250
$Comp
L Device:R_Small R?
U 1 1 631B0A30
P 4950 5550
AR Path="/61833839/631B0A30" Ref="R?"  Part="1" 
AR Path="/622F77ED/631B0A30" Ref="R?"  Part="1" 
AR Path="/622F77ED/6310EED9/631B0A30" Ref="R902"  Part="1" 
F 0 "R902" V 4754 5550 50  0000 C CNN
F 1 "1k" V 4845 5550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4950 5550 50  0001 C CNN
F 3 "~" H 4950 5550 50  0001 C CNN
	1    4950 5550
	0    1    -1   0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 631B0A2A
P 4950 6250
AR Path="/61833839/631B0A2A" Ref="R?"  Part="1" 
AR Path="/622F77ED/631B0A2A" Ref="R?"  Part="1" 
AR Path="/622F77ED/6310EED9/631B0A2A" Ref="R903"  Part="1" 
F 0 "R903" V 4754 6250 50  0000 C CNN
F 1 "1k" V 4845 6250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4950 6250 50  0001 C CNN
F 3 "~" H 4950 6250 50  0001 C CNN
	1    4950 6250
	0    1    -1   0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 631B0A24
P 5700 6250
AR Path="/61833839/631B0A24" Ref="R?"  Part="1" 
AR Path="/622F77ED/631B0A24" Ref="R?"  Part="1" 
AR Path="/622F77ED/6310EED9/631B0A24" Ref="R904"  Part="1" 
F 0 "R904" V 5504 6250 50  0000 C CNN
F 1 "1k" V 5595 6250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5700 6250 50  0001 C CNN
F 3 "~" H 5700 6250 50  0001 C CNN
	1    5700 6250
	0    1    -1   0   
$EndComp
Wire Wire Line
	5950 5900 5950 6250
Wire Wire Line
	5550 6200 5550 6550
Connection ~ 5950 5900
Wire Wire Line
	5150 5650 5150 5550
Connection ~ 5150 5550
Wire Wire Line
	5150 5550 5050 5550
$Comp
L project_power:GNDHR #PWR0904
U 1 1 61817B4D
P 4750 6350
F 0 "#PWR0904" H 4750 6100 50  0001 C CNN
F 1 "GNDHR" H 4755 6177 50  0000 C CNN
F 2 "" H 4750 6350 50  0001 C CNN
F 3 "" H 4750 6350 50  0001 C CNN
	1    4750 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 3150 6450 3450
Wire Wire Line
	6350 3350 6550 3350
Wire Wire Line
	6550 3350 6550 3250
Connection ~ 6550 3250
Wire Wire Line
	4500 5850 4750 5850
$Comp
L Device:C_Small C?
U 1 1 61879BF7
P 5650 6850
AR Path="/61879BF7" Ref="C?"  Part="1" 
AR Path="/617B514C/61879BF7" Ref="C?"  Part="1" 
AR Path="/619065BF/61879BF7" Ref="C?"  Part="1" 
AR Path="/61908BA4/61879BF7" Ref="C?"  Part="1" 
AR Path="/6192AB24/61879BF7" Ref="C?"  Part="1" 
AR Path="/619332FF/61879BF7" Ref="C?"  Part="1" 
AR Path="/619B691C/61879BF7" Ref="C?"  Part="1" 
AR Path="/619BF925/61879BF7" Ref="C?"  Part="1" 
AR Path="/619C8940/61879BF7" Ref="C?"  Part="1" 
AR Path="/619BE637/61879BF7" Ref="C?"  Part="1" 
AR Path="/619D45F6/61879BF7" Ref="C?"  Part="1" 
AR Path="/619EA585/61879BF7" Ref="C?"  Part="1" 
AR Path="/622F77ED/6310EED9/61879BF7" Ref="C909"  Part="1" 
F 0 "C909" V 5650 6650 50  0000 C CNN
F 1 "100p" V 5650 7150 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5650 6850 50  0001 C CNN
F 3 "~" H 5650 6850 50  0001 C CNN
	1    5650 6850
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 61879C0B
P 5650 6700
AR Path="/61879C0B" Ref="C?"  Part="1" 
AR Path="/617B514C/61879C0B" Ref="C?"  Part="1" 
AR Path="/619065BF/61879C0B" Ref="C?"  Part="1" 
AR Path="/61908BA4/61879C0B" Ref="C?"  Part="1" 
AR Path="/6192AB24/61879C0B" Ref="C?"  Part="1" 
AR Path="/619332FF/61879C0B" Ref="C?"  Part="1" 
AR Path="/619B691C/61879C0B" Ref="C?"  Part="1" 
AR Path="/619BF925/61879C0B" Ref="C?"  Part="1" 
AR Path="/619C8940/61879C0B" Ref="C?"  Part="1" 
AR Path="/619BE637/61879C0B" Ref="C?"  Part="1" 
AR Path="/619D45F6/61879C0B" Ref="C?"  Part="1" 
AR Path="/619EA585/61879C0B" Ref="C?"  Part="1" 
AR Path="/622F77ED/6310EED9/61879C0B" Ref="C908"  Part="1" 
F 0 "C908" V 5650 6500 50  0000 C CNN
F 1 "10n" V 5650 7000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5650 6700 50  0001 C CNN
F 3 "~" H 5650 6700 50  0001 C CNN
	1    5650 6700
	0    1    1    0   
$EndComp
Wire Wire Line
	5750 6550 5750 6700
Connection ~ 5750 6700
Wire Wire Line
	5750 6700 5750 6850
$Comp
L project_power:GNDHR #PWR0916
U 1 1 61879C18
P 5750 6850
F 0 "#PWR0916" H 5750 6600 50  0001 C CNN
F 1 "GNDHR" H 5850 6700 50  0000 C CNN
F 2 "" H 5750 6850 50  0001 C CNN
F 3 "" H 5750 6850 50  0001 C CNN
	1    5750 6850
	1    0    0    -1  
$EndComp
Connection ~ 5750 6850
$Comp
L Device:C_Small C?
U 1 1 61879C01
P 5650 6550
AR Path="/61879C01" Ref="C?"  Part="1" 
AR Path="/617B514C/61879C01" Ref="C?"  Part="1" 
AR Path="/619065BF/61879C01" Ref="C?"  Part="1" 
AR Path="/61908BA4/61879C01" Ref="C?"  Part="1" 
AR Path="/6192AB24/61879C01" Ref="C?"  Part="1" 
AR Path="/619332FF/61879C01" Ref="C?"  Part="1" 
AR Path="/619B691C/61879C01" Ref="C?"  Part="1" 
AR Path="/619BF925/61879C01" Ref="C?"  Part="1" 
AR Path="/619C8940/61879C01" Ref="C?"  Part="1" 
AR Path="/619BE637/61879C01" Ref="C?"  Part="1" 
AR Path="/619D45F6/61879C01" Ref="C?"  Part="1" 
AR Path="/619EA585/61879C01" Ref="C?"  Part="1" 
AR Path="/622F77ED/6310EED9/61879C01" Ref="C907"  Part="1" 
F 0 "C907" V 5650 6350 50  0000 C CNN
F 1 "100n" V 5650 6850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5650 6550 50  0001 C CNN
F 3 "~" H 5650 6550 50  0001 C CNN
	1    5650 6550
	0    1    1    0   
$EndComp
Connection ~ 5550 6550
Wire Wire Line
	5550 6550 5550 6700
Connection ~ 5550 6700
Wire Wire Line
	5550 6700 5550 6850
$Comp
L project_power:-5VHR #PWR0910
U 1 1 6188882D
P 5550 6850
F 0 "#PWR0910" H 5550 6950 50  0001 C CNN
F 1 "-5VHR" H 5565 7023 50  0000 C CNN
F 2 "" H 5550 6850 50  0001 C CNN
F 3 "" H 5550 6850 50  0001 C CNN
	1    5550 6850
	-1   0    0    1   
$EndComp
Connection ~ 5550 6850
$EndSCHEMATC
