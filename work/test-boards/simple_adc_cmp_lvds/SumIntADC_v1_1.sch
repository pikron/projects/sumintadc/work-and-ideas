EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "SumIntADC"
Date "2021-08-06"
Rev "1.1"
Comp "PiKRON, s.r.o."
Comment1 "Píša, Porazil, Ladman"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:MCP6001-OT U1
U 1 1 610D2529
P 4850 2650
F 0 "U1" H 4500 2650 50  0000 L CNN
F 1 "MCP6021T-E/OT" H 4550 1750 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4750 2450 50  0001 L CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20001685E.pdf" H 4850 2850 50  0001 C CNN
	1    4850 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R9
U 1 1 610D565D
P 4200 2350
F 0 "R9" H 4200 2500 50  0000 L CNN
F 1 "100k" V 4300 2250 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4200 2350 50  0001 C CNN
F 3 "~" H 4200 2350 50  0001 C CNN
	1    4200 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R7
U 1 1 610D58FB
P 4000 2350
F 0 "R7" H 4000 2500 50  0000 L CNN
F 1 "18k" V 4100 2250 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4000 2350 50  0001 C CNN
F 3 "~" H 4000 2350 50  0001 C CNN
	1    4000 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R3
U 1 1 610D5BA4
P 3800 2750
F 0 "R3" V 3604 2750 50  0000 C CNN
F 1 "33k" V 3695 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 3800 2750 50  0001 C CNN
F 3 "~" H 3800 2750 50  0001 C CNN
	1    3800 2750
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R1
U 1 1 610D6345
P 3300 2900
F 0 "R1" H 3100 2850 50  0000 L CNN
F 1 "49R9" H 3050 2950 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 3300 2900 50  0001 C CNN
F 3 "~" H 3300 2900 50  0001 C CNN
	1    3300 2900
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R4
U 1 1 610D6895
P 3800 3150
F 0 "R4" V 3604 3150 50  0000 C CNN
F 1 "33k" V 3695 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 3800 3150 50  0001 C CNN
F 3 "~" H 3800 3150 50  0001 C CNN
	1    3800 3150
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R13
U 1 1 610D6C38
P 5400 2650
F 0 "R13" V 5204 2650 50  0000 C CNN
F 1 "49R9" V 5295 2650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5400 2650 50  0001 C CNN
F 3 "~" H 5400 2650 50  0001 C CNN
	1    5400 2650
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R15
U 1 1 610D6FF3
P 5750 2400
F 0 "R15" H 5809 2446 50  0000 L CNN
F 1 "1k" H 5809 2355 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5750 2400 50  0001 C CNN
F 3 "~" H 5750 2400 50  0001 C CNN
	1    5750 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R16
U 1 1 610D746D
P 5750 3200
F 0 "R16" H 5809 3246 50  0000 L CNN
F 1 "1k" H 5809 3155 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5750 3200 50  0001 C CNN
F 3 "~" H 5750 3200 50  0001 C CNN
	1    5750 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT_TRIM RV1
U 1 1 610DB290
P 5750 2850
F 0 "RV1" H 5680 2804 50  0000 R CNN
F 1 "10k" H 5680 2895 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3266W_Vertical" H 5750 2850 50  0001 C CNN
F 3 "~" H 5750 2850 50  0001 C CNN
	1    5750 2850
	1    0    0    1   
$EndComp
$Comp
L Device:C_Small C11
U 1 1 610DCD5D
P 6000 3200
F 0 "C11" H 6092 3246 50  0000 L CNN
F 1 "10n" H 6092 3155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6000 3200 50  0001 C CNN
F 3 "~" H 6000 3200 50  0001 C CNN
	1    6000 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C9
U 1 1 610DE559
P 4950 3050
F 0 "C9" V 4721 3050 50  0000 C CNN
F 1 "220p" V 4812 3050 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4950 3050 50  0001 C CNN
F 3 "~" H 4950 3050 50  0001 C CNN
	1    4950 3050
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C1
U 1 1 610DEF17
P 4550 3150
F 0 "C1" V 4321 3150 50  0000 C CNN
F 1 "220p" V 4412 3150 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4550 3150 50  0001 C CNN
F 3 "~" H 4550 3150 50  0001 C CNN
	1    4550 3150
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C5
U 1 1 610DF512
P 4850 2350
F 0 "C5" V 4850 2150 50  0000 C CNN
F 1 "220p" V 4850 2650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4850 2350 50  0001 C CNN
F 3 "~" H 4850 2350 50  0001 C CNN
	1    4850 2350
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C3
U 1 1 610DFACE
P 4850 2050
F 0 "C3" V 4850 1850 50  0000 C CNN
F 1 "100n" V 4850 2350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4850 2050 50  0001 C CNN
F 3 "~" H 4850 2050 50  0001 C CNN
	1    4850 2050
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C4
U 1 1 610E0A00
P 4850 2200
F 0 "C4" V 4850 2000 50  0000 C CNN
F 1 "10n" V 4850 2500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4850 2200 50  0001 C CNN
F 3 "~" H 4850 2200 50  0001 C CNN
	1    4850 2200
	0    1    1    0   
$EndComp
Wire Wire Line
	4400 2550 4400 2450
Wire Wire Line
	4400 2550 4550 2550
Wire Wire Line
	4400 2550 4400 3150
Wire Wire Line
	4400 3150 4450 3150
Connection ~ 4400 2550
Wire Wire Line
	3900 3150 4400 3150
Connection ~ 4400 3150
Wire Wire Line
	4550 2750 4200 2750
Wire Wire Line
	4750 3300 4750 3150
Wire Wire Line
	4850 3050 4200 3050
Wire Wire Line
	4200 3050 4200 2750
Connection ~ 4200 2750
Wire Wire Line
	4200 2750 4000 2750
Wire Wire Line
	4200 2750 4200 2450
Wire Wire Line
	4000 2450 4000 2750
Connection ~ 4000 2750
Wire Wire Line
	4000 2750 3900 2750
$Comp
L power:GND #PWR06
U 1 1 610F322D
P 4750 3300
F 0 "#PWR06" H 4750 3050 50  0001 C CNN
F 1 "GND" H 4755 3127 50  0000 C CNN
F 2 "" H 4750 3300 50  0001 C CNN
F 3 "" H 4750 3300 50  0001 C CNN
	1    4750 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 610F37AF
P 5750 3300
F 0 "#PWR012" H 5750 3050 50  0001 C CNN
F 1 "GND" H 5755 3127 50  0000 C CNN
F 2 "" H 5750 3300 50  0001 C CNN
F 3 "" H 5750 3300 50  0001 C CNN
	1    5750 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 610F3B27
P 6000 3300
F 0 "#PWR015" H 6000 3050 50  0001 C CNN
F 1 "GND" H 6005 3127 50  0000 C CNN
F 2 "" H 6000 3300 50  0001 C CNN
F 3 "" H 6000 3300 50  0001 C CNN
	1    6000 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 2650 5200 2650
Wire Wire Line
	5500 2650 6350 2650
Wire Wire Line
	6000 2850 6000 3100
Wire Wire Line
	5900 2850 6000 2850
Connection ~ 6000 2850
Wire Wire Line
	6000 2850 6350 2850
Wire Wire Line
	5750 3100 5750 3000
Wire Wire Line
	5750 2700 5750 2500
Wire Wire Line
	4750 2350 4750 2200
Connection ~ 4750 2350
Connection ~ 4750 2200
Wire Wire Line
	4750 2200 4750 2050
Wire Wire Line
	4950 2050 4950 2200
Connection ~ 4950 2200
Wire Wire Line
	4950 2200 4950 2350
$Comp
L power:GND #PWR09
U 1 1 610F71E6
P 4950 2350
F 0 "#PWR09" H 4950 2100 50  0001 C CNN
F 1 "GND" H 4955 2177 50  0000 C CNN
F 2 "" H 4950 2350 50  0001 C CNN
F 3 "" H 4950 2350 50  0001 C CNN
	1    4950 2350
	1    0    0    -1  
$EndComp
Connection ~ 4950 2350
Wire Wire Line
	4000 2250 4000 1700
Wire Wire Line
	4000 1700 6350 1700
Wire Wire Line
	4200 2250 4200 2050
Wire Wire Line
	4200 2050 4300 2050
Wire Wire Line
	4400 2050 4400 2250
$Comp
L power:+2V5 #PWR03
U 1 1 610F8795
P 4300 2050
F 0 "#PWR03" H 4300 1900 50  0001 C CNN
F 1 "+2V5" H 4315 2223 50  0000 C CNN
F 2 "" H 4300 2050 50  0001 C CNN
F 3 "" H 4300 2050 50  0001 C CNN
	1    4300 2050
	1    0    0    -1  
$EndComp
Connection ~ 4300 2050
Wire Wire Line
	4300 2050 4400 2050
$Comp
L power:+2V5 #PWR011
U 1 1 610F916F
P 5750 2050
F 0 "#PWR011" H 5750 1900 50  0001 C CNN
F 1 "+2V5" H 5765 2223 50  0000 C CNN
F 2 "" H 5750 2050 50  0001 C CNN
F 3 "" H 5750 2050 50  0001 C CNN
	1    5750 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2300 5750 2050
Wire Wire Line
	5050 3050 5200 3050
Wire Wire Line
	5200 3050 5200 2650
Connection ~ 5200 2650
Wire Wire Line
	5200 2650 5300 2650
$Comp
L Device:R_Small R11
U 1 1 610D4F6D
P 4400 2350
F 0 "R11" H 4450 2500 50  0000 L CNN
F 1 "100k" V 4500 2250 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4400 2350 50  0001 C CNN
F 3 "~" H 4400 2350 50  0001 C CNN
	1    4400 2350
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J6
U 1 1 61109875
P 7900 3700
F 0 "J6" H 7792 3075 50  0000 C CNN
F 1 "Conn_01x08_Female" H 7792 3166 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x08_P2.54mm_Vertical" H 7900 3700 50  0001 C CNN
F 3 "~" H 7900 3700 50  0001 C CNN
	1    7900 3700
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x05_Female J5
U 1 1 6110D6AB
P 7900 2250
F 0 "J5" H 7750 2650 50  0000 L CNN
F 1 "Conn_01x05_Female" H 7400 2550 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x05_P2.54mm_Vertical" H 7900 2250 50  0001 C CNN
F 3 "~" H 7900 2250 50  0001 C CNN
	1    7900 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR017
U 1 1 61113836
P 7350 2450
F 0 "#PWR017" H 7350 2200 50  0001 C CNN
F 1 "GND" H 7355 2277 50  0000 C CNN
F 2 "" H 7350 2450 50  0001 C CNN
F 3 "" H 7350 2450 50  0001 C CNN
	1    7350 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 61116437
P 3100 3300
F 0 "#PWR01" H 3100 3050 50  0001 C CNN
F 1 "GND" H 3105 3127 50  0000 C CNN
F 2 "" H 3100 3300 50  0001 C CNN
F 3 "" H 3100 3300 50  0001 C CNN
	1    3100 3300
	1    0    0    -1  
$EndComp
Connection ~ 3100 3150
$Comp
L Connector:Conn_Coaxial J3
U 1 1 61139DDF
P 3300 2550
F 0 "J3" V 3600 2500 50  0000 L CNN
F 1 "5-1814832-2" V 3500 2300 50  0000 L CNN
F 2 "Connector_Coaxial:SMA_Amphenol_132291_Vertical" H 3300 2550 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Customer+Drawing%7F1814832%7FC%7Fpdf%7FEnglish%7FENG_CD_1814832_C.pdf%7F5-1814832-2" H 3300 2550 50  0001 C CNN
	1    3300 2550
	0    1    -1   0   
$EndComp
Wire Wire Line
	3300 2800 3300 2750
Text Label 7450 3300 0    50   ~ 0
HVCC
Text Label 7450 3400 0    50   ~ 0
P505
Text Label 7450 3500 0    50   ~ 0
N505
Text Label 7450 3600 0    50   ~ 0
P501
Text Label 7450 3700 0    50   ~ 0
N501
Text Label 7450 3800 0    50   ~ 0
P503
Text Label 7450 3900 0    50   ~ 0
N503
Text Label 7450 4000 0    50   ~ 0
GND
$Comp
L power:GND #PWR019
U 1 1 6116D0FF
P 7350 4100
F 0 "#PWR019" H 7350 3850 50  0001 C CNN
F 1 "GND" H 7355 3927 50  0000 C CNN
F 2 "" H 7350 4100 50  0001 C CNN
F 3 "" H 7350 4100 50  0001 C CNN
	1    7350 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 4100 7350 4000
Wire Wire Line
	7350 4000 7700 4000
$Comp
L power:+2V5 #PWR018
U 1 1 6116F167
P 7350 3200
F 0 "#PWR018" H 7350 3050 50  0001 C CNN
F 1 "+2V5" H 7365 3373 50  0000 C CNN
F 2 "" H 7350 3200 50  0001 C CNN
F 3 "" H 7350 3200 50  0001 C CNN
	1    7350 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 3200 7350 3300
Wire Wire Line
	7350 3300 7700 3300
$Comp
L power:PWR_FLAG #FLG01
U 1 1 61173CEF
P 7350 3300
F 0 "#FLG01" H 7350 3375 50  0001 C CNN
F 1 "PWR_FLAG" V 7350 3427 50  0000 L CNN
F 2 "" H 7350 3300 50  0001 C CNN
F 3 "~" H 7350 3300 50  0001 C CNN
	1    7350 3300
	0    -1   -1   0   
$EndComp
Connection ~ 7350 3300
$Comp
L power:PWR_FLAG #FLG02
U 1 1 6117586B
P 7350 4000
F 0 "#FLG02" H 7350 4075 50  0001 C CNN
F 1 "PWR_FLAG" V 7350 4127 50  0000 L CNN
F 2 "" H 7350 4000 50  0001 C CNN
F 3 "~" H 7350 4000 50  0001 C CNN
	1    7350 4000
	0    -1   -1   0   
$EndComp
Connection ~ 7350 4000
Wire Wire Line
	7700 3400 7350 3400
Wire Wire Line
	7350 3500 7700 3500
Wire Wire Line
	7350 3600 7700 3600
Wire Wire Line
	7350 3700 7700 3700
Wire Wire Line
	7350 3800 7700 3800
Wire Wire Line
	7350 3900 7700 3900
$Comp
L power:+2V5 #PWR05
U 1 1 6118830E
P 4750 2050
F 0 "#PWR05" H 4750 1900 50  0001 C CNN
F 1 "+2V5" H 4765 2223 50  0000 C CNN
F 2 "" H 4750 2050 50  0001 C CNN
F 3 "" H 4750 2050 50  0001 C CNN
	1    4750 2050
	1    0    0    -1  
$EndComp
Connection ~ 4750 2050
Wire Wire Line
	3700 3150 3100 3150
Wire Wire Line
	3100 3150 3100 3300
Wire Wire Line
	4650 3150 4750 3150
Connection ~ 4750 3150
Wire Wire Line
	4750 3150 4750 2950
$Comp
L pmod:PMOD-Device-x2-Type-XHS-Alt P1
U 1 1 611AAB2E
P 8200 5300
F 0 "P1" H 8300 6180 60  0000 C CNN
F 1 "PMOD-Device-x2-Type-XHS-Alt" H 8300 6090 39  0000 C CNN
F 2 "pmod-conn_6x2:pmod_pin_array_6x2" H 8300 5190 39  0001 C CNN
F 3 "" H 8300 5600 60  0000 C CNN
	1    8200 5300
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR021
U 1 1 611B6849
P 7550 5300
F 0 "#PWR021" H 7550 5050 50  0001 C CNN
F 1 "GND" H 7555 5127 50  0000 C CNN
F 2 "" H 7550 5300 50  0001 C CNN
F 3 "" H 7550 5300 50  0001 C CNN
	1    7550 5300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR022
U 1 1 611B6F7D
P 8600 5300
F 0 "#PWR022" H 8600 5050 50  0001 C CNN
F 1 "GND" H 8605 5127 50  0000 C CNN
F 2 "" H 8600 5300 50  0001 C CNN
F 3 "" H 8600 5300 50  0001 C CNN
	1    8600 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 5150 7550 5150
Wire Wire Line
	7550 5150 7550 5300
Wire Wire Line
	8500 5150 8600 5150
Wire Wire Line
	8600 5150 8600 5300
$Comp
L power:+2V5 #PWR020
U 1 1 611BB2BE
P 7450 5250
F 0 "#PWR020" H 7450 5100 50  0001 C CNN
F 1 "+2V5" H 7465 5423 50  0000 C CNN
F 2 "" H 7450 5250 50  0001 C CNN
F 3 "" H 7450 5250 50  0001 C CNN
	1    7450 5250
	1    0    0    -1  
$EndComp
$Comp
L power:+2V5 #PWR023
U 1 1 611BDE87
P 8700 5250
F 0 "#PWR023" H 8700 5100 50  0001 C CNN
F 1 "+2V5" H 8715 5423 50  0000 C CNN
F 2 "" H 8700 5250 50  0001 C CNN
F 3 "" H 8700 5250 50  0001 C CNN
	1    8700 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 5250 8500 5250
Wire Wire Line
	7450 5250 7700 5250
Text Label 7450 4950 0    50   ~ 0
P505
Text Label 8600 4950 0    50   ~ 0
N505
Text Label 7450 4750 0    50   ~ 0
P501
Text Label 7450 4850 0    50   ~ 0
N501
Text Label 8600 4750 0    50   ~ 0
P503
Text Label 8600 4850 0    50   ~ 0
N503
Wire Wire Line
	8850 4750 8500 4750
Wire Wire Line
	8500 4850 8850 4850
Wire Wire Line
	7350 4750 7700 4750
Wire Wire Line
	7350 4850 7700 4850
Wire Wire Line
	7350 4950 7700 4950
Wire Wire Line
	8500 4950 8850 4950
$Comp
L Amplifier_Operational:MCP6001-OT U2
U 1 1 6120A77E
P 4850 4900
F 0 "U2" H 4500 4900 50  0000 L CNN
F 1 "MCP6021T-E/OT" H 4550 4000 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4750 4700 50  0001 L CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20001685E.pdf" H 4850 5100 50  0001 C CNN
	1    4850 4900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R10
U 1 1 6120AA2E
P 4200 4600
F 0 "R10" H 4200 4750 50  0000 L CNN
F 1 "100k" V 4300 4500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4200 4600 50  0001 C CNN
F 3 "~" H 4200 4600 50  0001 C CNN
	1    4200 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R8
U 1 1 6120AA38
P 4000 4600
F 0 "R8" H 4000 4750 50  0000 L CNN
F 1 "18k" V 4100 4500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4000 4600 50  0001 C CNN
F 3 "~" H 4000 4600 50  0001 C CNN
	1    4000 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R5
U 1 1 6120AA42
P 3800 5000
F 0 "R5" V 3604 5000 50  0000 C CNN
F 1 "33k" V 3695 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 3800 5000 50  0001 C CNN
F 3 "~" H 3800 5000 50  0001 C CNN
	1    3800 5000
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R2
U 1 1 6120AA4C
P 3300 5150
F 0 "R2" H 3450 5150 50  0000 L CNN
F 1 "49R9" H 3400 5250 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 3300 5150 50  0001 C CNN
F 3 "~" H 3300 5150 50  0001 C CNN
	1    3300 5150
	1    0    0    1   
$EndComp
$Comp
L Device:R_Small R6
U 1 1 6120AA56
P 3800 5400
F 0 "R6" V 3604 5400 50  0000 C CNN
F 1 "33k" V 3695 5400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 3800 5400 50  0001 C CNN
F 3 "~" H 3800 5400 50  0001 C CNN
	1    3800 5400
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R14
U 1 1 6120AA60
P 5400 4900
F 0 "R14" V 5204 4900 50  0000 C CNN
F 1 "49R9" V 5295 4900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5400 4900 50  0001 C CNN
F 3 "~" H 5400 4900 50  0001 C CNN
	1    5400 4900
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R17
U 1 1 6120AA6A
P 5750 4650
F 0 "R17" H 5809 4696 50  0000 L CNN
F 1 "1k" H 5809 4605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5750 4650 50  0001 C CNN
F 3 "~" H 5750 4650 50  0001 C CNN
	1    5750 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R18
U 1 1 6120AA74
P 5750 5450
F 0 "R18" H 5809 5496 50  0000 L CNN
F 1 "1k" H 5809 5405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5750 5450 50  0001 C CNN
F 3 "~" H 5750 5450 50  0001 C CNN
	1    5750 5450
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT_TRIM RV2
U 1 1 6120AA7E
P 5750 5100
F 0 "RV2" H 5680 5054 50  0000 R CNN
F 1 "10k" H 5680 5145 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3266W_Vertical" H 5750 5100 50  0001 C CNN
F 3 "~" H 5750 5100 50  0001 C CNN
	1    5750 5100
	1    0    0    1   
$EndComp
$Comp
L Device:C_Small C12
U 1 1 6120AA88
P 6000 5450
F 0 "C12" H 6092 5496 50  0000 L CNN
F 1 "10n" H 6092 5405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6000 5450 50  0001 C CNN
F 3 "~" H 6000 5450 50  0001 C CNN
	1    6000 5450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C10
U 1 1 6120AA92
P 4950 5300
F 0 "C10" V 4721 5300 50  0000 C CNN
F 1 "220p" V 4812 5300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4950 5300 50  0001 C CNN
F 3 "~" H 4950 5300 50  0001 C CNN
	1    4950 5300
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C2
U 1 1 6120AA9C
P 4550 5400
F 0 "C2" V 4321 5400 50  0000 C CNN
F 1 "220p" V 4412 5400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4550 5400 50  0001 C CNN
F 3 "~" H 4550 5400 50  0001 C CNN
	1    4550 5400
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C8
U 1 1 6120AAA6
P 4850 4600
F 0 "C8" V 4850 4400 50  0000 C CNN
F 1 "220p" V 4850 4900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4850 4600 50  0001 C CNN
F 3 "~" H 4850 4600 50  0001 C CNN
	1    4850 4600
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C6
U 1 1 6120AAB0
P 4850 4300
F 0 "C6" V 4850 4100 50  0000 C CNN
F 1 "100n" V 4850 4600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4850 4300 50  0001 C CNN
F 3 "~" H 4850 4300 50  0001 C CNN
	1    4850 4300
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C7
U 1 1 6120AABA
P 4850 4450
F 0 "C7" V 4850 4250 50  0000 C CNN
F 1 "10n" V 4850 4750 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4850 4450 50  0001 C CNN
F 3 "~" H 4850 4450 50  0001 C CNN
	1    4850 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	4400 4800 4400 4700
Wire Wire Line
	4400 4800 4550 4800
Wire Wire Line
	4400 4800 4400 5400
Wire Wire Line
	4400 5400 4450 5400
Connection ~ 4400 4800
Wire Wire Line
	3900 5400 4400 5400
Connection ~ 4400 5400
Wire Wire Line
	4550 5000 4200 5000
Wire Wire Line
	4750 5550 4750 5400
Wire Wire Line
	4850 5300 4200 5300
Wire Wire Line
	4200 5300 4200 5000
Connection ~ 4200 5000
Wire Wire Line
	4200 5000 4000 5000
Wire Wire Line
	4200 5000 4200 4700
Wire Wire Line
	4000 4700 4000 5000
Connection ~ 4000 5000
Wire Wire Line
	4000 5000 3900 5000
$Comp
L power:GND #PWR08
U 1 1 6120AAD5
P 4750 5550
F 0 "#PWR08" H 4750 5300 50  0001 C CNN
F 1 "GND" H 4755 5377 50  0000 C CNN
F 2 "" H 4750 5550 50  0001 C CNN
F 3 "" H 4750 5550 50  0001 C CNN
	1    4750 5550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 6120AADF
P 5750 5550
F 0 "#PWR014" H 5750 5300 50  0001 C CNN
F 1 "GND" H 5755 5377 50  0000 C CNN
F 2 "" H 5750 5550 50  0001 C CNN
F 3 "" H 5750 5550 50  0001 C CNN
	1    5750 5550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 6120AAE9
P 6000 5550
F 0 "#PWR016" H 6000 5300 50  0001 C CNN
F 1 "GND" H 6005 5377 50  0000 C CNN
F 2 "" H 6000 5550 50  0001 C CNN
F 3 "" H 6000 5550 50  0001 C CNN
	1    6000 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 4900 5200 4900
Wire Wire Line
	5500 4900 6350 4900
Wire Wire Line
	6000 5100 6000 5350
Wire Wire Line
	5900 5100 6000 5100
Connection ~ 6000 5100
Wire Wire Line
	6000 5100 6350 5100
Wire Wire Line
	5750 5350 5750 5250
Wire Wire Line
	5750 4950 5750 4750
Wire Wire Line
	4750 4600 4750 4450
Connection ~ 4750 4600
Connection ~ 4750 4450
Wire Wire Line
	4750 4450 4750 4300
Wire Wire Line
	4950 4300 4950 4450
Connection ~ 4950 4450
Wire Wire Line
	4950 4450 4950 4600
$Comp
L power:GND #PWR010
U 1 1 6120AB02
P 4950 4600
F 0 "#PWR010" H 4950 4350 50  0001 C CNN
F 1 "GND" H 4955 4427 50  0000 C CNN
F 2 "" H 4950 4600 50  0001 C CNN
F 3 "" H 4950 4600 50  0001 C CNN
	1    4950 4600
	1    0    0    -1  
$EndComp
Connection ~ 4950 4600
Wire Wire Line
	4000 4500 4000 3950
Wire Wire Line
	4000 3950 6350 3950
Wire Wire Line
	4200 4500 4200 4300
Wire Wire Line
	4200 4300 4300 4300
Wire Wire Line
	4400 4300 4400 4500
$Comp
L power:+2V5 #PWR04
U 1 1 6120AB12
P 4300 4300
F 0 "#PWR04" H 4300 4150 50  0001 C CNN
F 1 "+2V5" H 4315 4473 50  0000 C CNN
F 2 "" H 4300 4300 50  0001 C CNN
F 3 "" H 4300 4300 50  0001 C CNN
	1    4300 4300
	1    0    0    -1  
$EndComp
Connection ~ 4300 4300
Wire Wire Line
	4300 4300 4400 4300
$Comp
L power:+2V5 #PWR013
U 1 1 6120AB1E
P 5750 4300
F 0 "#PWR013" H 5750 4150 50  0001 C CNN
F 1 "+2V5" H 5765 4473 50  0000 C CNN
F 2 "" H 5750 4300 50  0001 C CNN
F 3 "" H 5750 4300 50  0001 C CNN
	1    5750 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 4550 5750 4300
Wire Wire Line
	5050 5300 5200 5300
Wire Wire Line
	5200 5300 5200 4900
Connection ~ 5200 4900
Wire Wire Line
	5200 4900 5300 4900
$Comp
L Device:R_Small R12
U 1 1 6120AB2D
P 4400 4600
F 0 "R12" H 4450 4750 50  0000 L CNN
F 1 "100k" V 4500 4500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4400 4600 50  0001 C CNN
F 3 "~" H 4400 4600 50  0001 C CNN
	1    4400 4600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J4
U 1 1 6120AB44
P 3300 4800
F 0 "J4" V 3600 4750 50  0000 L CNN
F 1 "5-1814832-2" V 3500 4550 50  0000 L CNN
F 2 "Connector_Coaxial:SMA_Amphenol_132291_Vertical" H 3300 4800 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Customer+Drawing%7F1814832%7FC%7Fpdf%7FEnglish%7FENG_CD_1814832_C.pdf%7F5-1814832-2" H 3300 4800 50  0001 C CNN
	1    3300 4800
	0    1    -1   0   
$EndComp
$Comp
L power:+2V5 #PWR07
U 1 1 6120AB63
P 4750 4300
F 0 "#PWR07" H 4750 4150 50  0001 C CNN
F 1 "+2V5" H 4765 4473 50  0000 C CNN
F 2 "" H 4750 4300 50  0001 C CNN
F 3 "" H 4750 4300 50  0001 C CNN
	1    4750 4300
	1    0    0    -1  
$EndComp
Connection ~ 4750 4300
Wire Wire Line
	4650 5400 4750 5400
Connection ~ 4750 5400
Wire Wire Line
	4750 5400 4750 5200
Wire Wire Line
	7700 2450 7350 2450
Wire Wire Line
	7700 2050 7350 2050
Wire Wire Line
	7350 2050 7350 2150
Connection ~ 7350 2450
Wire Wire Line
	7700 2150 7350 2150
Connection ~ 7350 2150
Wire Wire Line
	7350 2150 7350 2250
Wire Wire Line
	7700 2250 7350 2250
Connection ~ 7350 2250
Wire Wire Line
	7350 2250 7350 2350
Wire Wire Line
	7700 2350 7350 2350
Connection ~ 7350 2350
Wire Wire Line
	7350 2350 7350 2450
Text Label 6150 1700 0    50   ~ 0
P505
Text Label 6150 3950 0    50   ~ 0
N505
Text Label 6150 2650 0    50   ~ 0
P501
Text Label 6150 2850 0    50   ~ 0
N501
Text Label 6150 4900 0    50   ~ 0
P503
Text Label 6150 5100 0    50   ~ 0
N503
NoConn ~ 7700 5050
NoConn ~ 8500 5050
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 6113BEC0
P 2850 2750
F 0 "J1" H 3300 2650 50  0000 C CNN
F 1 "Conn_01x02" H 3150 2750 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2850 2750 50  0001 C CNN
F 3 "~" H 2850 2750 50  0001 C CNN
	1    2850 2750
	-1   0    0    -1  
$EndComp
Connection ~ 3100 2850
Wire Wire Line
	3050 2750 3300 2750
Connection ~ 3300 2750
Wire Wire Line
	3050 3150 3100 3150
Wire Wire Line
	3300 3050 3300 3000
Wire Wire Line
	3050 2850 3100 2850
Wire Wire Line
	3100 2850 3100 3150
Wire Wire Line
	3100 2550 3100 2850
$Comp
L Connector_Generic:Conn_01x02 J7
U 1 1 61199DAB
P 2850 3050
F 0 "J7" H 3300 2950 50  0000 C CNN
F 1 "Conn_01x02" H 3150 3050 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2850 3050 50  0001 C CNN
F 3 "~" H 2850 3050 50  0001 C CNN
	1    2850 3050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3300 2750 3700 2750
Wire Wire Line
	3300 3050 3050 3050
Wire Wire Line
	3300 5000 3700 5000
$Comp
L power:GND #PWR02
U 1 1 611FE82B
P 3100 5550
F 0 "#PWR02" H 3100 5300 50  0001 C CNN
F 1 "GND" H 3105 5377 50  0000 C CNN
F 2 "" H 3100 5550 50  0001 C CNN
F 3 "" H 3100 5550 50  0001 C CNN
	1    3100 5550
	1    0    0    -1  
$EndComp
Connection ~ 3100 5400
Wire Wire Line
	3700 5400 3100 5400
Wire Wire Line
	3100 5400 3100 5550
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 611FEAC0
P 2850 5000
F 0 "J2" H 3300 4900 50  0000 C CNN
F 1 "Conn_01x02" H 3150 5000 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2850 5000 50  0001 C CNN
F 3 "~" H 2850 5000 50  0001 C CNN
	1    2850 5000
	-1   0    0    -1  
$EndComp
Connection ~ 3100 5100
Wire Wire Line
	3050 5000 3300 5000
Wire Wire Line
	3050 5400 3100 5400
Wire Wire Line
	3050 5100 3100 5100
Wire Wire Line
	3100 5100 3100 5400
Wire Wire Line
	3100 4800 3100 5100
$Comp
L Connector_Generic:Conn_01x02 J8
U 1 1 611FEAD0
P 2850 5300
F 0 "J8" H 3300 5200 50  0000 C CNN
F 1 "Conn_01x02" H 3150 5300 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2850 5300 50  0001 C CNN
F 3 "~" H 2850 5300 50  0001 C CNN
	1    2850 5300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3300 5300 3050 5300
Connection ~ 3300 5000
Wire Wire Line
	3300 5050 3300 5000
Wire Wire Line
	3300 5250 3300 5300
$Comp
L Connector_Generic:Conn_01x02 J9
U 1 1 61274475
P 8050 5400
F 0 "J9" V 8014 5212 50  0000 R CNN
F 1 "Conn_01x02" V 7850 5200 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 8050 5400 50  0001 C CNN
F 3 "~" H 8050 5400 50  0001 C CNN
	1    8050 5400
	0    -1   -1   0   
$EndComp
NoConn ~ 8050 5600
NoConn ~ 8150 5600
$EndSCHEMATC
