#!/opt/nx/nxmap-3/bin/nxpython

import sys
import subprocess

from os import path
from os import symlink

from nxmap import *
import NX1H35_EK_V1
kit = NX1H35_EK_V1.Kit()

baseDir = path.dirname(path.realpath(__file__))

sys.path.append(baseDir)

p = createProject(baseDir)
p.setTopCellName('sumint_nx_bb_v3')
p.setVariantName('NG-MEDIUM')

# SpaceWire
#p.addIP('IP_SPW_BANK')

p.addFile('src/sumint_nx_bb_v3.vhdl')
p.addFile('src/pdchain.vhdl')
p.addFile('src/pdivtwo.vhdl')
p.addFile('src/siroladc.vhdl')
p.addFile('src/util_pkg.vhdl')

p.setOptions({'MappingEffort': 'High',
              'TimingDriven': 'Yes', 		# (No) (Place/Route) If set to 'Yes', algorithms are timing driven (can be 'Yes' or 'No').  Setting "TimingDriven" option to Yes forces "MappingEffort" to High.
              'DefaultRAMMapping': 'RAM',	   # (RF) Default mapping of RAM (can be onto 'RF' or 'RAM')
              'DefaultROMMapping': 'RAM',
              'ManageAsynchronousReadPort': 'Yes',

              'MaxRegisterCount': '4300'
          })

#p.addMappingDirective('getModels(.*arom.*)', 'RAM', 'RAM')
#p.addMappingDirective('getModels(.*arom.*)', 'ROM', 'RAM')

def siadc_ain_pad_cfg(location):
    return {'location': location, 'standard': 'LVDS', 'drive':'Undefined', 'differential' : True,
             'weakTermination': 'None', 'terminationReference': 'Floating', 'turbo': False, 'inputDelayLine': 0}
def siadc_refsw_pad_cfg(location):
    return {'location': location, 'standard': 'LVCMOS', 'drive':'16mA'}

def siadc_cmosin_pad_cfg(location):
    return {'location': location, 'standard': 'LVCMOS',
            'weakTermination': 'None', 'terminationReference': 'Floating',
            'turbo': False, 'inputDelayLine': 0}

def spi_dac_pad_cfg(location):
    return {'location': location, 'standard': 'LVCMOS', 'drive':'16mA'}

def twsi_out_pad_cfg(location):
    return {'location': location, 'standard': 'LVCMOS', 'drive':'16mA'}

pads = {
    'osc0' : kit.pad('osc0'),

    # Simple SumIntADC channels (simple.sch)

    'siadc_ain[0]':    siadc_ain_pad_cfg('IOB4_D08P'),  # HD18P - LVDS IN 1 -
    'siadc_refsw[0]':  siadc_refsw_pad_cfg('IOB4_D02N'), # HD19N - Analog IN 2
    'siadc_refout[0]': siadc_refsw_pad_cfg('IOB4_D02P'), # HD19P - Reference source

    'siadc_ain[1]':    siadc_ain_pad_cfg('IOB5_D12P'),   # HD16P - LVDS IN 1 +
    'siadc_refsw[1]':  siadc_refsw_pad_cfg('IOB5_D14N'), # HD17N - Analog IN 1
    'siadc_refout[1]': siadc_refsw_pad_cfg('IOB5_D14P'), # HD17P - Reference source

    # Advanced SumIntADC channels (advanced.sch)

    'siadc_ain[2]':    siadc_cmosin_pad_cfg('IOB5_D11P'),   # HD15P - LVDS IN 1 +
    'siadc_refsw[2]':  siadc_refsw_pad_cfg('IOB2_D07P'), # HD7P - Analog IN 1

    'siadc_ain[3]':    siadc_ain_pad_cfg('IOB4_D04P'),   # HRX3P - LVDS IN 1 +
    'siadc_refsw[3]':  siadc_refsw_pad_cfg('IOB3_D13P'), # HD3P - Analog IN 1

    'siadc_ain[4]':    siadc_ain_pad_cfg('IOB5_D13P'),   # HRX4P - LVDS IN 1 +
    'siadc_refsw[4]':  siadc_refsw_pad_cfg('IOB2_D01P'), # HD6P - Analog IN 1

    'siadc_ain[5]':    siadc_ain_pad_cfg('IOB5_D10P'),   # HRX5P - LVDS IN 1 +
    'siadc_refsw[5]':  siadc_refsw_pad_cfg('IOB3_D05P'), # HD0P - Analog IN 1

    #  High Resolution SumIntADC channels (hires.sch)

    'siadc_hr_ain[0]':     siadc_ain_pad_cfg('IOB4_D10P'),   # HD20P - LVDS IN 1 +
    'siadc_hr_refsw_n[0]': siadc_refsw_pad_cfg('IOB4_D07P'), # HD21P - Analog IN 1

    'siadc_hr_ain[1]':     siadc_ain_pad_cfg('IOB4_D09P'),   # HD22P - LVDS IN 1 +
    'siadc_hr_refsw_n[1]': siadc_refsw_pad_cfg('IOB4_D13P'), # HD23P - Analog IN 1

    'spi_dac_clock':       spi_dac_pad_cfg('IOB4_D14N'),     # HD27N
    'spi_dac_cs':          spi_dac_pad_cfg('IOB4_D14P'),     # HD27P
    'spi_dac_mosi':        spi_dac_pad_cfg('IOB4_D15P'),     # HD25P
    'spi_dac_sel':         spi_dac_pad_cfg('IOB4_D15P'),     # HD25N

    # Two Wire Serial Interface

    #'twsi_do': twsi_out_pad_cfg('IOB12_D05P'), # PC05 TX1
    #'twsi_so': twsi_out_pad_cfg('IOB12_D06P'), # PC06 TX3

    'twsi_do': twsi_out_pad_cfg('IOB12_D05N'), # PC05 TX2
    'twsi_so': twsi_out_pad_cfg('IOB12_D06N'), # PC06 TX4

    # Kit Debuggin LEDs

    'led[0]' : kit.pad('LED1'),
    'led[1]' : kit.pad('LED2'),
    'led[2]' : kit.pad('LED3'),
    'led[3]' : kit.pad('LED4'),
    'led[4]' : kit.pad('LED5'),
    'led[5]' : kit.pad('LED6'),
    'led[6]' : kit.pad('LED7'),
    'led[7]' : kit.pad('LED8'),
    'btn[0]' : kit.pad('PB10'),
    'btn[1]' : kit.pad('PB11'),
}
p.addPads(pads)

#p.createClock('getClockNet(clock)', 'clock', 4000)

p.save('prj_imported.nym')

if not p.synthesize():
    p.destroy()
    sys.exit(1)

p.save('prj_synthesized.vhd')

if not p.place():
    p.destroy()
    sys.exit(1)

p.save('prj_placed.vhd')

if not p.route():
    p.destroy()
    sys.exit(1)

p.save('prj_routed.vhd')
p.save('prj_routed.sdf')
p.save('prj_routed.v')
p.save('prj_routed.nym')

if not p.generateBitstream('out.nxb'):
    p.destroy()
    sys.exit(1)

analyzer = p.createAnalyzer()
analyzer.launch()

p.destroy()
printText('*** Design successfully generated ***')
