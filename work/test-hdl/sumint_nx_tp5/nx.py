#!/opt/nx/nxmap-3/bin/nxpython

import sys
import subprocess

from os import path
from os import symlink

from nxmap import *
import NX1H35_EK_V1
kit = NX1H35_EK_V1.Kit()

baseDir = path.dirname(path.realpath(__file__))

sys.path.append(baseDir)

p = createProject(baseDir)
p.setTopCellName('sumint_nx_tp5')
p.setVariantName('NG-MEDIUM')

# SpaceWire
#p.addIP('IP_SPW_BANK')

p.addFile('src/sumint_nx_tp5.vhdl')
p.addFile('src/pdchain.vhdl')
p.addFile('src/pdivtwo.vhdl')
p.addFile('src/siroladc.vhdl')

p.setOptions({'MappingEffort': 'High',
              'TimingDriven': 'Yes', 		# (No) (Place/Route) If set to 'Yes', algorithms are timing driven (can be 'Yes' or 'No').  Setting "TimingDriven" option to Yes forces "MappingEffort" to High.
              'DefaultRAMMapping': 'RAM',	   # (RF) Default mapping of RAM (can be onto 'RF' or 'RAM')
              'DefaultROMMapping': 'RAM',
              'ManageAsynchronousReadPort': 'Yes',

              'MaxRegisterCount': '4300'
          })

#p.addMappingDirective('getModels(.*arom.*)', 'RAM', 'RAM')
#p.addMappingDirective('getModels(.*arom.*)', 'ROM', 'RAM')

def siadc_ain_pad_cfg(location):
    return {'location': location, 'standard': 'LVDS', 'drive':'Undefined', 'differential' : True,
             'weakTermination': 'None', 'terminationReference': 'Floating', ' turbo': False, 'inputDelayLine': 0}
def siadc_refsw_pad_cfg(location):
    return {'location': location, 'standard': 'LVCMOS', 'drive':'16mA'}

pads = {
    'osc0' : kit.pad('osc0'),

    'siadc_ain[0]':   siadc_ain_pad_cfg('IOB5_D01P'),   # LVDS IN 1 +
    'siadc_refsw[0]': siadc_refsw_pad_cfg('IOB5_D05P'), # Analog IN 1

    'siadc_ain[1]':   siadc_ain_pad_cfg('IOB5_D03P'),   # LVDS IN 1 -
    'siadc_refsw[1]': siadc_refsw_pad_cfg('IOB5_D05N'), # Analog IN 2

    'led[0]' : kit.pad('LED1'),
    'led[1]' : kit.pad('LED2'),
    'led[2]' : kit.pad('LED3'),
    'led[3]' : kit.pad('LED4'),
    'led[4]' : kit.pad('LED5'),
    'led[5]' : kit.pad('LED6'),
    'led[6]' : kit.pad('LED7'),
    'led[7]' : kit.pad('LED8'),
    'btn[0]' : kit.pad('PB10'),
    'btn[1]' : kit.pad('PB11'),
}
p.addPads(pads)

#p.createClock('getClockNet(clock)', 'clock', 4000)

p.save('prj_imported.nym')

if not p.synthesize():
    p.destroy()
    sys.exit(1)

p.save('prj_synthesized.vhd')

if not p.place():
    p.destroy()
    sys.exit(1)

p.save('prj_placed.vhd')

if not p.route():
    p.destroy()
    sys.exit(1)

p.save('prj_routed.vhd')
p.save('prj_routed.sdf')
p.save('prj_routed.v')
p.save('prj_routed.nym')

if not p.generateBitstream('out.nxb'):
    p.destroy()
    sys.exit(1)

analyzer = p.createAnalyzer()
analyzer.launch()

p.destroy()
printText('*** Design successfully generated ***')
