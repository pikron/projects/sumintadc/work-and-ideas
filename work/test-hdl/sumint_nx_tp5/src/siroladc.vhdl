--
-- * sigma-integral rolling ADC digital part *
-- ADC described in:
--  PISA, P.; PORAZIL, P. Suma-Integration Analog to Digital
--  Converter, Idea, Im-plementation and Results. In HORACEK, P.
--  SIMANDL, M. ZITEK, P. (Ed.) Preprints of the 16th World Congress
--  of the International Federation of Automatic Control, s. 1 6,
--  Prague, July 2005. IFAC.
--
-- part of LXPWR motion control board (c) PiKRON Ltd
-- idea by Pavel Pisa PiKRON Ltd <ppisa@pikron.com>
-- code by Pavel Pisa <ppisa@pikron.com>
-- based on cmladc by Marek Peca <mp@duch.cz>
-- 01/2013
--
-- license: GNU GPLv3
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity siroladc is
  generic (
    width: natural := 16
  );
  port (
    -- reset: in std_logic;
    clock: in std_logic;
    sync: in std_logic;
    pcounter_value: out std_logic_vector (width-1 downto 0);
    adc_mux: out std_logic;
    adc_cmp: in std_logic
  );
end siroladc;

architecture behavioral of siroladc is
  component pdchain is
    generic (
      n: natural
      );
    port (
      clock: in std_logic;
      en: in std_logic;
      q: out std_logic_vector (n-1 downto 0)
      );
  end component;

  signal q, next_q: std_logic;
begin
  pcounter: pdchain
    generic map (
      n => width)
    port map (
      clock => clock,
      en => q,
      q => pcounter_value);

  adc_mux <= q;

  -- edge <= last_q and not q;
  -- next_value_reg <= count when edge = '1' else value_reg;

  rs: process (sync, adc_cmp, q)
  begin
    next_q <= q;
    --
    -- assume sync is a 1-clock pulse
    -- prioritize reset (=sync) over set (=adc_cmp)
    --
    if sync = '1' then
      next_q <= '1';
    elsif adc_cmp = '0' then
      next_q <= '0';
    end if;
  end process;

--   next_q <= (not adc_cmp and q) or sync;

  seq: process
  begin
    wait until clock'event and clock = '1';
    q <= next_q;
    -- last_q <= q;
  end process;
end behavioral;
