library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--use work.util.all;

-- NXmap v2.9.5 (NXmap3 2020.1 had a LVDS out bug)

library NX;
use NX.nxPackage.all;

entity sumint_nx_tp5 is
  generic (
    n_adc: natural := 2;
    adc_width: natural := 16;
    adc_cycle: natural := 500 -- 125
    );
  port (
    osc0 : in std_logic;
    --
    siadc_ain : in std_logic_vector (n_adc - 1 downto 0);
    siadc_refsw : out std_logic_vector (n_adc - 1 downto 0);
    --
    btn  : in std_logic_vector (1 downto 0);
    led  : out std_logic_vector (7 downto 0)
    );
end sumint_nx_tp5;

architecture rtl of sumint_nx_tp5 is
  component siroladc is
    generic (
      width: natural
    );
    port (
      -- reset: in std_logic;
      clock: in std_logic;
      sync: in std_logic;
      pcounter_value: out std_logic_vector (width-1 downto 0);
      adc_mux: out std_logic;
      adc_cmp: in std_logic
    );
  end component;

  signal clock, clock_x2, clock_x4, pll_lock, reset_sync: std_logic;
  signal rst_reg: std_logic_vector (2 downto 0);

  type adc_matrix_t is array (0 to n_adc-1) of
    std_logic_vector (adc_width-1 downto 0);
  signal adc_word: adc_matrix_t;
  signal last_adc_word: adc_matrix_t;
  signal prev_adc_word: adc_matrix_t;
  signal adc_mon_word: adc_matrix_t;
  signal next_adc_mon_word: adc_matrix_t;
  signal clock_adc_mod: std_logic;
  signal adc_cycle_cnt: integer range 0 to adc_cycle-1;
  signal adc_sync, next_adc_sync: std_logic;
  signal sync2fast, last_sync2fast, next_sync2fast: std_logic;

begin
  --
  -- osc0=25MHz, f_vco=400MHz, clock = 100MHz, clock_x2 = 200MHz, clock_x4 = 400MHz
  pll0: NX_PLL
    generic map (
                                                --    0: 200 -> 425 MHz
                                                --    1: 400 -> 850 MHz
      vco_range    => 1,                        --    2: 800 -> 1200 MHz
                                                -- No added %2 division
      ref_div_on   => '0',                      -- bypass :: %2
      fbk_div_on   => '0',                      -- bypass :: %2
      ext_fbk_on   => '0',
                                                -- if fbk_div_on = 0
                                                --   2 to 31 => %4 to %62
                                                -- else
      fbk_intdiv   => 8,                        --   1 to 15 => %4 to %60
                                                -- No Fbk Delay
      fbk_delay_on => '0',                      -- fbk delay not used ...
      fbk_delay    => 0,                        -- 0 to 63
      --clk_outdiv1  => 3, -- *16/8 =  50MHz      -- 0 to 7   (%1 to %2^7)
      clk_outdiv1  => 2, -- *16/4 = 100MHz      -- 0 to 7   (%1 to %2^7)
      clk_outdiv2  => 1, -- *16/1 = 200MHz      -- 0 to 7   (%1 to %2^7)
      clk_outdiv3  => 0  -- *16/2 = 400MHz      -- 0 to 7   (%1 to %2^7)
    )
    port map (
      REF  => osc0,
      FBK  => OPEN,
      VCO  => OPEN,
      D1   => clock,
      D2   => clock_x2,
      D3   => clock_x4,
      OSC  => OPEN,
      RDY  => pll_lock
      );


  clock_adc_mod <= clock_x4;

  reset_gen: process (pll_lock, rst_reg, clock)
  begin
    if pll_lock = '0' then
      rst_reg <= "111";
      reset_sync <= '1';
    elsif clock'event and clock = '1' then
      rst_reg <= rst_reg(1 downto 0) & (not pll_lock);
      reset_sync <= rst_reg(2) or rst_reg(1);
    end if;
  end process;

  adc_block: for i in n_adc-1 downto 0 generate
    adc: siroladc
      generic map (width => adc_width)
      port map (
        clock => clock_adc_mod,
        sync => adc_sync,
        pcounter_value => adc_word(i)(adc_width-1 downto 0),
        adc_mux => siadc_refsw(i),
        adc_cmp => siadc_ain(i)
      );
  end generate;

  --adc_conn: for i in 0 to n_adc-1 generate
  --  adc_data(i*sio_adc_len + adc_width-1
  --           downto i*sio_adc_len) <= adc_word(i);
  --  adc_data(i*sio_adc_len + 2 * adc_width-3
  --           downto i*sio_adc_len + adc_width) <= last_adc_word(i)(adc_width-3 downto 0);
  --end generate;

  adc_monitor: for i in n_adc-1 downto 0 generate
    next_adc_mon_word(i) <= std_logic_vector(unsigned(last_adc_word(i)) -
                            unsigned(prev_adc_word(i)));
  end generate;

  next_adc_sync <= not last_sync2fast and sync2fast;

  adc_last: process(clock, adc_word, reset_sync)
  begin
    if reset_sync = '1' then
      adc_cycle_cnt <= 0;
      next_sync2fast <= '0';
      last_adc_word <= ((others=> (others=>'0')));
      prev_adc_word <= ((others=> (others=>'0')));
      adc_mon_word <= ((others=> (others=>'0')));
    elsif clock'event and clock = '1' then
      if adc_cycle_cnt = 0 then
        adc_cycle_cnt <= adc_cycle - 1;
        prev_adc_word <= last_adc_word;
        last_adc_word <= adc_word;
        adc_mon_word <= next_adc_mon_word;
        next_sync2fast <= '1';
      else
        adc_cycle_cnt <= adc_cycle_cnt - 1;
        next_sync2fast <= '0';
      end if;
    end if;
  end process;

  fast_domain_sync: process (clock_adc_mod, reset_sync, sync2fast, next_sync2fast, next_adc_sync)
  begin
    if reset_sync = '1' then
      sync2fast <= '0';
    elsif clock_adc_mod'event and clock_adc_mod = '1' then
      sync2fast <= next_sync2fast;
      last_sync2fast <= sync2fast;
      adc_sync <= next_adc_sync;
    end if;
  end process;

  led(7 downto 0) <= not adc_mon_word(0)(8 downto 1);

  --led(0) <= not di;
  --led(1) <= not si;
  --led(2) <= not bool_to_std(dbg_null);
  --led(3) <= not bool_to_std(rx_valid);
  --led(4) <= not bool_to_std(dbg_err);
  --led(6 downto 5) <= not rxd(1 downto 0);
  --led(7) <= not rx_clk;
end architecture;
