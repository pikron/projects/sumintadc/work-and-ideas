Automated Testing of the SumIntADC
==================================

This software package provides automation of the static and dynamic characterization of the [SumIntADC](https://gitlab.com/pikron/projects/sumintadc/work-and-ideas/-/wikis/home).

organization: [Czech Technical University in Prague](https://www.cvut.cz/), [Faculty of Electrical Engineering](https://fel.cvut.cz/cz/), [Department of Measurement](https://meas.fel.cvut.cz/)

author: © 2022 Michal Spacek - spacemi6@fel.cvut.cz

advisors: Radek Sedlacek - sedlacr@fel.cvut.cz, Josef Vedral - vedral@fel.cvut.cz

funded by PiKRON s.r.o. - http://www.pikron.com

## Instrumentation Setup

* 2x 8,5-digit digital multimeter Keysight 3458A
* measurement/source unit Keysight B2912A
* low distortion generator Stanford Research Systems DS 360
* climate chamber, type ClimaEvent C/180/70/3
* PT100 (as temperature sensor) connected to the Agilent DMM 34410
* PiKRON MZ_APO Xilinx Zynq XC7Z010 base system - twsi_capt_1.0 IP core

## Device under Test

* nanoXplore NX1H35AS-EK development kit
* [SumIntADC NX breadboard](https://gitlab.com/pikron/projects/sumintadc/work-and-ideas/-/wikis/sumint-breadboard-v1)
