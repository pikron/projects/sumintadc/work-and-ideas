"""
    Automated testing of the SumIntADC

    adc_dynamic.py - master script for dynamic tests of the ADC

    organization: Czech Technical University in Prague, Faculty of Electrical Engineering, Department of Measurement
    author: © 2022 Michal Spacek - spacemi6@fel.cvut.cz
    advisors: Radek Sedlacek - sedlacr@fel.cvut.cz, Josef Vedral - vedral@fel.cvut.cz
    funded by PiKRON s.r.o. - http://www.pikron.com
"""

from agilent3458a import Agilent3458A
from srs_ds360 import SrsDs360
from agilent34410 import Agilent34410
from heat_chamber import HeatChamber
import constants as const

from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation as fa
import numpy as np
from datetime import datetime as dt
import pandas as pd
import time
from os import path, getcwd, mkdir
from progress.bar import Bar
import socket

class AdcDynamicTest:
    def __init__(self):
        self.dmm_sig = Agilent3458A('GPIB0::10::INSTR')
        self.dmm_ref = Agilent3458A('GPIB0::22::INSTR')
        self.lfg = SrsDs360('GPIB0::8::INSTR')
        self.tm = Agilent34410('GPIB0::1::INSTR')
        self.hm = HeatChamber()

    def get_unique_file_name(self, file_path, base_name):
        file_indx = 0
        file_name = base_name % (file_indx)
        while path.isfile(path.join(file_path, file_name)):
            file_name = base_name % (file_indx)
            file_indx += 1
        return file_name

    def get_unique_dir_name(self, dir_path, base_name):
        dir_indx = 0
        dir_name = base_name % (dir_indx)
        while path.isdir(path.join(dir_path, dir_name)):
            dir_indx += 1
            dir_name = base_name % (dir_indx)
        return dir_name

    def save_meas_info(self, amplitude, frequency, amb_temperature, mm_temperature, temperature, humidity):
        file_name = "measurement_info.txt"
        file_path = path.join(self.meas_path, file_name)
        message_str =   "%s\n\
                        \rambient temperature: %f °C\n\
                        \rmultimeter temperature: %f °C\n\
                        \rchamber temperature: %f °C\n\
                        \rchamber humidity: %f %%\n\
                        \rchannel: %d \n\
                        \rblind channel: %d \n\
                        \ramplitude: %f V\n\
                        \rfrequency: %f Hz\n\
                        \radc measurement argument: %s" % (str(dt.now()), amb_temperature, mm_temperature, temperature, humidity, const.CHANNEL, const.BLIND_CHANNEL, amplitude, frequency, const.CMD_ARG.decode('utf-8').rstrip("\n"))
        f = open(file_path, "w")
        f.write(message_str)
        f.close()

    def init_directory(self):
        if path.isdir(path.join(getcwd(), "dynamic_measurement_data")) != True:
            mkdir(path.join(getcwd(), "dynamic_measurement_data"))
        dir_name = self.get_unique_dir_name(path.join(getcwd(), "dynamic_measurement_data"), "dynamic_meas_%d")
        self.meas_path = path.join(getcwd(), "dynamic_measurement_data", dir_name)
        mkdir(self.meas_path)

    def save_mm_data(self, measured_data):
        df = pd.DataFrame({'measured_values': measured_data})
        df.to_csv(path.join(self.meas_path, "mm_data.csv"), index=False, header=True)

    def save_adc_data(self, data, subsample_data, blind, subsample_blind):
        df = pd.DataFrame({'sample_values': data, 'subsample_values': subsample_data, 'blind_values': blind, 'subblind_values': subsample_blind})
        df.to_csv(path.join(self.meas_path, "adc_data.csv"), index=False, header=True)

    def frequency_sweep(self, amplitude, frequency_set, settle_time, temperature, humidity):
        progress_bar = Bar('Measuring', max=len(frequency_set))

        self.lfg.set_amplitude(amplitude)
        self.lfg.enable_output(True)
        for frequency in frequency_set:
            self.init_directory()
            self.save_meas_info(amplitude, frequency, self.tm.read_temp(), self.dmm_sig.get_temp(), temperature, humidity)

            self.lfg.set_frequency(frequency)
            time.sleep(settle_time)

            self.dmm_sig.setup_dcv_sampling(const.NUM_OF_DMM_READINGS)
            self.dmm_sig.start_dcv_sampling()

            final_state = None
            while final_state is None:
                final_state = self.grab_adc_data(2 * const.CHANNEL, 2 * const.BLIND_CHANNEL)

            self.save_mm_data(self.dmm_sig.read_dcv_samples(const.NUM_OF_DMM_READINGS))

            progress_bar.next()
        self.lfg.enable_output(False)
        progress_bar.finish()

    def grab_adc_data(self, channel, blind_channel):
        self.sck = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sck.connect(const.ADC_ADDRESS)
        self.sck.sendall(const.CMD_ARG)
        receive_ready = self.sck.makefile('r')
        line = receive_ready.readline().rstrip("\n")
        if len(line) == 0:
            print("No response from ADC")
            return None, None, None
        first = True
        while True:
            line = receive_ready.readline().rstrip("\n")
            if len(line) == 0:
                break
            value_sample = np.asarray(line.split("\t"), dtype=np.double)
            value_sample = np.reshape(value_sample, (1, len(value_sample)))
            if first:
                first = False
                value_series = value_sample
            else:
                value_series = np.append(value_series, value_sample, axis=0)
        self.sck.close()

        if first:
            return None

        if np.shape(value_series)[0] < 3000:
            return None
        else:
            self.save_adc_data(value_series[:, channel], value_series[:, channel + 1], value_series[:, blind_channel], value_series[:, blind_channel + 1])
            return 0

if __name__ == "__main__":
    adc_dynamic = AdcDynamicTest()
    if const.ENV_CONDITIONS is not None:
        adc_dynamic.hm.set_manual_mode(True)
        for temp, hum in const.ENV_CONDITIONS:
            adc_dynamic.hm.set_temperature_setpoint(temp)
            adc_dynamic.hm.set_humidity_setpoint(hum)
            while adc_dynamic.hm.values_in_limits(1, 30) == False:
                print("Preparing desired conditions", end="\r")
            print("\nTemperature settling")
            time.sleep(const.TEMPERATURE_SETTLE_TIME)
            adc_dynamic.frequency_sweep(const.AMPLITUDE, const.FREQUENCY_SET, const.SETTLE_TIME, temp, hum)
        adc_dynamic.hm.set_manual_mode(False)
    else:
        adc_dynamic.frequency_sweep(const.AMPLITUDE, const.FREQUENCY_SET, const.SETTLE_TIME, adc_dynamic.hm.get_actual_temperature(), adc_dynamic.hm.get_actual_humidity())
