"""
    Automated testing of the SumIntADC

    agilent3458a.py - control script for DMM Agilent 3458A

    organization: Czech Technical University in Prague, Faculty of Electrical Engineering, Department of Measurement
    author: © 2022 Michal Spacek - spacemi6@fel.cvut.cz
    advisors: Radek Sedlacek - sedlacr@fel.cvut.cz, Josef Vedral - vedral@fel.cvut.cz
    funded by PiKRON s.r.o. - http://www.pikron.com
"""

import pyvisa as pv
import struct as st
from matplotlib import pyplot as plt
from scipy.signal import blackman, boxcar, flattop, hamming, kaiser
import numpy as np
import pandas as pd
from tqdm import tqdm
from matplotlib.animation import FuncAnimation
from datetime import datetime as dt

class Agilent3458A:
    def __init__(self, gpib_name):
        rm = pv.ResourceManager()
        self.rd = rm.open_resource(gpib_name)
        self.rd.write_termination = '\r\n'
        self.rd.read_termination = '\r\n'
        self.rd.timeout = 50000
        self.dev_reset()

    def dev_reset(self):
        self.rd.write('PRESET NORM')

    def get_dcv_q(self):
        return self.rd.query('DCV')

    def get_dcv_w(self):
        self.rd.write('DCV')

    def get_dcv_r(self):
        return self.rd.read()

    def get_temp(self):
        return float(self.rd.query('TEMP?'))

    def get_id(self):
        return self.rd.query('ID?')

    def make_beep(self):
        self.rd.write('TONE')

    def disp_ctrl(self, on=True, message=None, clear=False):
        if on == True and message == None and clear == False:
            self.rd.write('DISP ON')
        elif on == False and message == None and clear == False:
            self.rd.write('DISP OFF')
        elif on == True and message != None and clear == False:
            self.rd.write('DISP MSG ' + message)
        elif on == True and message == None and clear == True:
            self.rd.write('DISP CLR')

    def get_mem_cnt(self):
        return self.rd.query('MCOUNT?')

    def set_memory(self):
        self.rd.write('MEM 2')

    def conv_voltage(self, data, byte_num=2):
        unpacked_data = []
        if byte_num == 2:
            unpacked_data = float(self.scale) * st.unpack(">h", data)[0]
        elif byte_num == 4:
            unpacked_data = float(self.scale) * st.unpack(">i", data)[0]
        return unpacked_data

    def conv_buffer(self, data, byte_num=2):
        return [self.conv_voltage(i, byte_num) for i in data]

    def setup_for_dc_measurement(self, range):
        self.rd.write('DCV ' + str(range))
        self.rd.write('APER 100E-3')

    def setup_dcv_sampling(self, num_of_samples):
        self.rd.write('RESET')
        self.rd.write('PRESET DIG')
        self.rd.write('INBUF ON')
        self.rd.write('AZERO OFF')
        self.rd.write('MEM FIFO')
        self.rd.write('DCV 1')
        self.rd.write('TIMER 2E-5')
        self.rd.write('APER 10E-6')
        self.rd.write('DELAY 5E-7')
        self.rd.write('MFORMAT DINT')
        self.rd.write('OFORMAT DINT')

        self.rd.write('TRIG LEVEL')
        self.rd.write('LEVEL 0 DC')
        self.rd.write('NRDGS ' + str(num_of_samples) + ' TIMER')
        self.scale = self.rd.query('ISCALE?')

    def start_dcv_sampling(self):
        self.rd.write('TARM SGL')

    def read_dcv_samples(self, num_of_samples):
        data_buff = []
        for _ in tqdm(range(0, num_of_samples), desc="Measuring..."):
            data_buff.append(self.rd.read_bytes(4))
        return self.conv_buffer(data_buff, byte_num=4)

    def get_samples_dcv(self, num_of_samples, init=False):
        if init:
            self.setup_dcv_sampling(num_of_samples)
        self.start_dcv_sampling()
        return self.read_dcv_samples(num_of_samples)

    def get_multiple_chunks(self, num_of_chunks, num_of_samples=37888, use_win=False):
        data_buff = []
        if use_win:
            w = kaiser(num_of_samples, 16)
        else:
            w = boxcar(num_of_samples)
        for i in range(0, num_of_chunks):
            if i == 0:
                data_buff.extend(self.conv_buffer(self.get_samples_dcv(num_of_samples, init=True), 4) * w)
            else:
                data_buff.extend(self.conv_buffer(self.get_samples_dcv(num_of_samples), 4) * w)
            print("chunk %d done" % (i))
        return data_buff

    def export_data(self, data):
        df = pd.DataFrame({'data': data})
        df.to_csv("dmm_data.csv", index=False)

    def plot_conditions(self):
        self.act_time, self.act_temp = [], []
        self.figure = plt.figure()
        self.line, = plt.plot_date(self.act_time, self.act_temp, '-o')
        plt.xlabel("time [-]")
        plt.ylabel("t [°C]")
        plt.title('Multimeter temperature')
        plt.grid(visible=True, which='major', color='k', linestyle='-', alpha=0.2)
        plt.grid(visible=True, which='minor', color='k', linestyle='--', alpha=0.1)
        plt.minorticks_on()
        animation = FuncAnimation(self.figure, self.update, interval=1000)
        plt.show()

    def update(self, frame):
        self.act_time.append(dt.now())
        self.act_temp.append(self.get_temp())

        self.line.set_data(self.act_time, self.act_temp)
        self.figure.gca().relim()
        self.figure.gca().autoscale_view()
        return self.line,

if __name__ == "__main__":
    mm = Agilent3458A('GPIB0::22::INSTR')
