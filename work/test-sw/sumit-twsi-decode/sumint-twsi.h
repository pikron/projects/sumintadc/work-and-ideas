/*******************************************************************

  Simple code to decode SumInt ADC data from TWSI capture

  Base on rdwrmem.c, Copyright 2004 - 2017 by Pavel Pisa

  (C) Copyright 2004 - 2022 by Pavel Pisa
      e-mail:   ppisa@pikron.com
      project:  https://gitlab.com/pikron/projects/sumintadc/
      personal: http://cmp.felk.cvut.cz/~pisa
      work:     http://www.pikron.com/
      license:  any combination GPL, LGPL, MPL or BSD licenses

 *******************************************************************/

#ifndef SUMINT_TWSI_H
#define SUMINT_TWSI_H

#include <stdint.h>

#include "mmap_file.h"

#define SUMIT_TWSI_HEADER_MAGIC 0xf5

#define SUMIT_TWSI_HEADER_o   0
#define SUMIT_TWSI_MSGID_o    1
#define SUMIT_TWSI_SEQ_o      2
#define SUMIT_TWSI_LEN_o      3
#define SUMIT_TWSI_DATA_o     4

#define SUMIT_TWSI_MSGID_ADC  0x10

typedef struct sumint_twsi_raw {
  mmaped_file_region_t *mfr;
  void *frames_first;
  void *frames_end;
  int frame_size;
  int frame_count;
  void *mem_start;
} sumint_twsi_raw_t;

static inline uint16_t sumint_twsi_get_u16_le(void *ptr)
{
  uint8_t *p = (uint8_t *)ptr;
  return p[0] + ((uint16_t)p[1] << 8);
}

int sumint_twsi_raw_open(sumint_twsi_raw_t *twsiraw, const char *fname);

int sumint_twsi_in_memory(sumint_twsi_raw_t *twsiraw, void *mem_start, size_t mem_size);

int sumint_twsi_check_frame(void *frame, void *frames_end, int frames_try);

int sumint_twsi_raw_setup_frames(sumint_twsi_raw_t *twsiraw, int validate);

int sumint_twsi_raw_values_print(FILE *file, sumint_twsi_raw_t *twsiraw, unsigned int mask);

int sumint_twsi_filtered_values_print(FILE *file, sumint_twsi_raw_t *twsiraw, unsigned int mask, int decimation, int options);

enum sumint_twsi_filtered_values_print_options {
  SUMINT_TWSI_PRTOPT_CSV = 1,
};

#endif /*SUMINT_TWSI_H*/