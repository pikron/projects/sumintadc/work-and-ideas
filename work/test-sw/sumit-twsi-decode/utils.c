/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware 

  utils.c - utilities for reading numbers and parsing of text line

  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <stdint.h>
//#include <system_def.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

/* string parse and input routines */

/**
 * si_skspace - Skip space and blank characters
 * @ps:		Pointer to character poiter into parsed string
 *
 * Functions checks character **@ps by isspace() category and
 * moves *@ps pointer after last blank character. If there is at least one
 * blank character, returns 1, else returns 0.
 */
int si_skspace(char **ps)
{
  char *p=*ps;
  if (!isspace((uint8_t)*(p++))) return 0;
  while(isspace((uint8_t)*(p++)));
  *ps=p-1;
  return 1;
}

/**
 * si_fndsep - Finds field separator character
 * @ps:		Pointer to character pointer into parsed string
 * @sepchrs:	String of possible separator characters.
 *
 * Function skips blanks, then checks, if next character belongs
 * to @sepchrs list. If character belongs, function returns found
 * character and *@ps points after separator else returns -1.
 */
int si_fndsep(char **ps,char *sepchrs)
{
  char c;
  si_skspace(ps);
  c=**ps;
  if(!c) return 0;
  if(!strchr(sepchrs,c)) return -1;
  (*ps)++;
  return c;
}

/**
 * si_alnumn - Reads alphanumeric value
 * @ps:		Pointer to character pointer into parsed string
 * @pout:	Pointer to read value buffer
 * @n:		Maximal number of read characters.
 *
 * Function reads alphanumeric characters and stores them in @pout,
 * if there is no more alphanumeric characters or @n is reached,
 * it returns count of stored characters.
 */
int si_alnumn(char **ps,char *pout,int n)
{
  char *p=*ps;
  char c;
  int ret=0;
  while(isalnum((uint8_t)(c=*p))||(*p=='_')) {
    p++;
    *(pout++)=c;
    if(++ret==n) break;
  }
  *pout=0;
  *ps=p;
  return ret;
}

/**
 * si_alphan - Reads alphabetic value
 * @ps:		Pointer to character pointer into parsed string
 * @pout:	Pointer to read value buffer
 * @n:		Maximal number of read characters.
 *
 * Function reads alphabetic characters and stores them in @pout,
 * if there is no more alphabetic characters or @n is reached,
 * it returns count of stored characters.
 */
int si_alphan(char **ps,char *pout,int n)
{
  char *p=*ps;
  char c;
  int ret=0;
  while(isalpha((uint8_t)(c=*p))) {
    p++;
    *(pout++)=c;
    if(++ret==n) break;
  }
  *pout=0;
  *ps=p;
  return ret;
}

/**
 * si_long - Reads numeric value
 * @ps:		Pointer to character pointer into parsed string
 * @val:	Pointer to long integer value buffer
 * @base:	Base of converted number, 0 means select by C rules
 *
 * Function reads digits and converts them to longint value.
 * It stops at end of the string or if non digit character is reached.
 * Returns 1 for successful conversion or -1 if no character can be
 * converted. Pointer *@ps paints after last character belonging to
 * numeric input.
 */
int si_long(char **ps,long *val,int base)
{
  char *p;
  *val=strtol(*ps,&p,base);
  if(*ps==p) return -1;
  *ps=p;
  return 1;
}

/**
 * si_ulong - Reads unsigned numeric value
 * @ps:		Pointer to character pointer into parsed string
 * @val:	Pointer to long integer value buffer
 * @base:	Base of converted number, 0 means select by C rules
 *
 * Function reads digits and converts them to longint value.
 * It stops at end of the string or if non digit character is reached.
 * Returns 1 for successful conversion or -1 if no character can be
 * converted. Pointer *@ps paints after last character belonging to
 * numeric input.
 */
int si_ulong(char **ps,long *val,int base)
{
  char *p;
  *val=strtoul(*ps,&p,base);
  if(*ps==p) return -1;
  *ps=p;
  return 1;
}

