#define _DEFAULT_SOURCE
#define _BSD_SOURCE

#include <stdint.h>
#include <stdatomic.h>
#include <inttypes.h>
#include <stdlib.h>
#include <unistd.h>

#include "log.h"
#include "hw.h"
#include "dma_setup.h"

int dma_do_transfer(const dma_setup_t *dma_setup,
                    void (*enable_source)(void), void (*disable_source)(void),
                    volatile uint32_t *len_reg, volatile uint32_t *shadow_len_reg,
                    dma_save_data_fnc_t save_data)
{
    uint32_t mem_addrp = DMA_ADDR;
    size_t block_len_w = DMA_MAX_LEN_W;
    size_t total_len_w = dma_setup->transfer_size_w;
    bool first = true;

    log_wr(L_INFO, "total_len=%" PRIu32 ", block_len=%" PRIu32 "\n",
           total_len_w*WORD_SIZE, block_len_w*WORD_SIZE);

    /* DMA reset */
    uint32_t tmp;
    tmp = mm_ctrl[TWSI_CAPTURE_REG_CR];
    tmp |= TWSI_CAPTURE_CR_FIFO_RST | TWSI_CAPTURE_CR_DMAFSM_RST;
    // reset rxdma core + fifo
    mm_ctrl[TWSI_CAPTURE_REG_CR] = tmp;
    __mb();
    usleep(1);
    tmp &= ~(TWSI_CAPTURE_CR_FIFO_RST | TWSI_CAPTURE_CR_DMAFSM_RST);
    mm_ctrl[TWSI_CAPTURE_REG_CR] = tmp;
    __mb();

    log_wr(L_INFO, "resetting DMA ...");
    dma_s2mm_reg_wr(XILINX_DMA_REG_DMACR, XILINX_DMA_DMACR_RESET);
    log_wr(L_INFO, "waiting for reset clear ...");
    while ((dma_s2mm_reg_rd(XILINX_DMA_REG_DMACR) & XILINX_DMA_DMACR_RESET))
        usleep(1);
    log_wr(L_INFO, "DMA reset clear");

    /*
     * The core will repeat transfers with the same length.
     * The length is loaded into a shadow register on the beginning of
     * the transfer, so we must setup the length of the last transfer
     * immediately after starting the one before that.
     */
    while (total_len_w && !g_quit) {
        bool last = total_len_w <= block_len_w;
        bool next_to_last = total_len_w <= 2*block_len_w && !last;

        if (last) {
            block_len_w = total_len_w;
            if (first)
                rx_dma_trigger(block_len_w-1);
            rx_dma_untrigger();
        }
        if (first && !last) {
            rx_dma_trigger(block_len_w-1);
        }
        if (next_to_last) {
            rx_dma_trigger(total_len_w - block_len_w - 1);
        }

        log_wr(L_INFO, "DMA read loop: %" PRIu32 " bytes", block_len_w*WORD_SIZE);
        tmp = mm_ctrl[TWSI_CAPTURE_REG_SR];
        if (tmp & TWSI_CAPTURE_SR_XRUN)
            log_wr(L_WARN, "FIFO Overrun. Some data will be missing.");

        /* prepare DMA engine */
        dma_s2mm_reg_wr(XILINX_DMA_REG_DMACR, XILINX_DMA_DMACR_RUNSTOP);  /* S2MM_DMACR.RS = 1 */
        dma_s2mm_reg_wr(XILINX_DMA_REG_ADDR, mem_addrp); /* S2MM_DA = addr. */
        dma_s2mm_reg_wr(XILINX_DMA_REG_ADDR_MSB, 0); /* S2MM_DA_MSB = 0. */
        dma_s2mm_reg_wr(XILINX_DMA_REG_LENGTH, block_len_w * WORD_SIZE); /* S2MM_LENGTH = len [B] */
        //log_wr(L_DEBUG, "DMA: CR=0x%08x, ADDR=%");

        if (first)
          enable_source();

        first = false;

        total_len_w -= block_len_w;

        log_wr(L_INFO, "waiting for DMA ready");
        while (!g_quit) {
            uint32_t sr = dma_s2mm_reg_rd(XILINX_DMA_REG_DMASR);
            if (sr & XILINX_DMA_DMASR_IDLE)
                break;
            else if (sr & XILINX_DMA_DMASR_HALTED) {
                log_wr(L_ERR, "DMA Halted - DMA error? (SR=0x%08x)", sr);
                dump_regs();
                return -1;
            }
            usleep(10);
        }
        if (g_quit) {
            /*
             * - disable zlogan
             * - zlogan will flush its output
             * - DMA transfer will fail (because the configured transfer size
             *   does not match with received data burst length)
             * - either read written data length from zlogan or detect it by
             *   inspecting the transferred bytes
             * - save only the transferred data
             */
            uint32_t len, shadow_len;
            bool ok = false;
            log_wr(L_INFO, "interrupted mid-burst, tearing down ...");
            disable_source();
            for (int i=0; i<1000; ++i) {
                uint32_t sr = dma_s2mm_reg_rd(XILINX_DMA_REG_DMASR);
                if (sr & (XILINX_DMA_DMASR_IDLE | XILINX_DMA_DMASR_HALTED)) {
                    ok = true;
                    break;
                }
                usleep(100);
            }
            if (!ok)
                log_wr(L_WARN, "DMA did not finish in time, giving up");
            __mb();
            len = *len_reg;
            shadow_len = *shadow_len_reg;

            block_len_w = len - shadow_len;
        }
        log_wr(L_INFO, "writing %u bytes ...", block_len_w*WORD_SIZE);
        save_data(dma_setup, (const void *) mm_dma_data, block_len_w*WORD_SIZE);
    }
    disable_source();
    return 0;
}
