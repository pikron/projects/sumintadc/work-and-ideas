#ifndef DMA_SETUP_H
#define DMA_SETUP_H

#include <stdint.h>
#include <stdatomic.h>
#include <signal.h>
#include <stdbool.h>

#include "log.h"
#include "hw.h"

extern unsigned WORD_SIZE; // detected at runtime
extern volatile sig_atomic_t g_quit;

// NOTE: the DMA core has apparently some address alignment limitation
#define DMA_MAX_LEN_W     (((1U << DMA_LENGTH_BITS)/WORD_SIZE - 1) & ~0x0)
#define DMA_MAX_LEN_B     (DMA_MAX_LEN_W*WORD_SIZE)
/*
#define DMA_MAX_LEN_W     ((1U << (DMA_LENGTH_BITS-1))/WORD_SIZE)
#define DMA_MAX_LEN_B     (DMA_MAX_LEN_W*WORD_SIZE)
*/

typedef struct dma_setup_t {
    size_t transfer_size_w;
    FILE *out;
} dma_setup_t;

//------------------------------------------------------------------------------

static inline void dma_reg_wr(unsigned reg, uint32_t val) {
    volatile uint32_t *p = &((volatile uint32_t*)mm_dma_ctl)[reg/4];
    log_wr(L_DEBUG, "dma_reg_wr(0x%02x): *0x%08x = 0x%08x", reg, (unsigned)p, val);
    __mb();
    *p = val;
    __mb();
}

static inline uint32_t dma_reg_rd(unsigned reg) {
    volatile uint32_t *p = &((volatile uint32_t*)mm_dma_ctl)[reg/4];
    __mb();
    uint32_t val = *p;
    __mb();
    //log_wr(L_DEBUG, "dma_reg_rd(0x%02x): *0x%08x == 0x%08x", reg, (unsigned)p, val);
    return val;
}

static inline void dma_s2mm_reg_wr(unsigned reg, uint32_t val) {
    dma_reg_wr(XILINX_DMA_S2MM_CTRL_OFFSET+reg, val);
}

static inline uint32_t dma_s2mm_reg_rd(unsigned reg) {
    return dma_reg_rd(XILINX_DMA_S2MM_CTRL_OFFSET+reg);
}

static inline void rx_dma_trigger(uint32_t dma_len) {
    log_wr(L_DEBUG, "rx_dma_trigger(%u)", dma_len);
    mm_ctrl[TWSI_CAPTURE_REG_LEN] = dma_len; // fill length
    __mb();
    mm_ctrl[TWSI_CAPTURE_REG_CR] |= TWSI_CAPTURE_CR_DMAFSM_TRIG;
    __mb();
}

static inline void rx_dma_untrigger() {
    log_wr(L_DEBUG, "rx_dma_untrigger()");
    __mb();
    mm_ctrl[TWSI_CAPTURE_REG_CR] &= ~TWSI_CAPTURE_CR_DMAFSM_TRIG;
    __mb();
}

typedef int (*dma_save_data_fnc_t)(const dma_setup_t *dma_setup, const void *data, size_t size);

int dma_do_transfer(const dma_setup_t *dma_setup,
                    void (*enable_source)(void), void (*disable_source)(void),
                    volatile uint32_t *len_reg, volatile uint32_t *shadow_len_reg,
                    dma_save_data_fnc_t save_data);

void dump_regs();

#endif /*DMA_SETUP_H*/