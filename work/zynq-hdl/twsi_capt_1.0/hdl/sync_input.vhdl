--
-- sampling of external asynchronous input
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sync_input is
  generic (
    n_resync: natural := 2  -- n of resync DFFs to reach given MTBF
  );
  port (
    clock: in std_logic;
    d: in std_logic;
    q: out std_logic
  );
end sync_input;

architecture rtl of sync_input is
  signal y: std_logic_vector (n_resync downto 0);
  attribute REGISTER_DUPLICATION : string;
  attribute REGISTER_DUPLICATION of y : signal is "NO";
begin
  y(y'high) <= d;
  q <= y(0);

  seq: process
  begin
    wait until rising_edge(clock);
    y(y'high-1 downto 0) <= y(y'high downto 1);
  end process;

end rtl;
