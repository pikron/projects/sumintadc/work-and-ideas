--
-- Two Wire Serial Interface Channel receiver
-- Copyright (c) 2022 Pavel Pisa <ppisa@pikron.com>
--
-- Code is based on the Zlogan (Logic Analyzer) project by Marek Peca
--
-- High-throughput ProtoBuf(RLE(delta_time | input)) Logic Analyzer
-- Copyright (c) 2017 Marek Peca <mp@eltvor.cz>
--
-- This source file is free software; you can redistribute it and/or
-- modify it under the terms of the GNU Lesser General Public
-- License as published by the Free Software Foundation; either
-- version 2.1 of the License, or (at your option) any later version.
--
-- This source file is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
-- Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public
-- License along with this library; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity twsi_capt_v1_0 is
  generic (
    -- Users to add parameters here
    la_b_out: natural := 4; -- output FIFO width (4 or 8 [bytes])
    -- User parameters ends
    -- Do not modify the parameters beyond this line

    -- Parameters of Axi Master Bus Interface M00_AXIS
    C_M00_AXIS_TDATA_WIDTH  : integer  := 32;
    C_M00_AXIS_START_COUNT  : integer  := 32
  );
  port (
    -- Two Wire Serial Interface Channel
    twsi_di              : in std_logic;
    twsi_si              : in std_logic;

    fifo_data_count_i : in std_logic_vector(31 downto 0);
    fifo_wr_data_count_i : in std_logic_vector(31 downto 0);
    fifo_rd_data_count_i : in std_logic_vector(31 downto 0);

    fifo_reset_n : out std_logic;

    timestamp            : in  std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);

    -- Ports of APB Interface
    aclk                 : in  std_logic;
    arstn                : in  std_logic;
    s_apb_paddr          : in  std_logic_vector(31 downto 0);
    s_apb_penable        : in  std_logic;
    s_apb_pprot          : in  std_logic_vector(2 downto 0);
    s_apb_prdata         : out std_logic_vector(31 downto 0);
    s_apb_pready         : out std_logic;
    s_apb_psel           : in  std_logic;
    s_apb_pslverr        : out std_logic;
    s_apb_pstrb          : in  std_logic_vector(3 downto 0);
    s_apb_pwdata         : in  std_logic_vector(31 downto 0);
    s_apb_pwrite         : in  std_logic;

    -- Ports of Axi Master Bus Interface M00_AXIS
    m00_axis_aclk        : in std_logic;
    m00_axis_aresetn     : in std_logic;
    m00_axis_tvalid      : out std_logic;
    m00_axis_tdata       : out std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
    m00_axis_tstrb       : out std_logic_vector((C_M00_AXIS_TDATA_WIDTH/8)-1 downto 0);
    m00_axis_tlast       : out std_logic;
    m00_axis_tready      : in std_logic
  );
end twsi_capt_v1_0;

architecture arch_imp of twsi_capt_v1_0 is
  -- component declaration
  component twsi_capt_apb is
    generic (
      S2MM_TDATA_WIDTH : integer
    );
    port (
      aclk                 : in  std_logic;
      arstn                : in  std_logic;

      reg_enable_o         : out std_logic;
      reg_dma_trig_o       : out std_logic;
      reg_dma_reset_o      : out std_logic;
      reg_dma_len_o        : out unsigned(29 downto 0);
      reg_dma_xrun_i       : in  std_logic;
      reg_state_mon_i      : in  std_logic_vector(2 downto 0);
      reg_count_mon_i      : in  unsigned(29 downto 0);
      reg_la_reset_o       : out std_logic;
      fifo_data_count_i    : in  std_logic_vector(31 downto 0);
      fifo_wr_data_count_i : in  std_logic_vector(31 downto 0);
      fifo_rd_data_count_i : in  std_logic_vector(31 downto 0);
      fifo_reset_n_o       : out std_logic;

      frame_header_i       : in  std_logic_vector(31 downto 0);

      s_apb_paddr          : in  std_logic_vector(31 downto 0);
      s_apb_penable        : in  std_logic;
      s_apb_pprot          : in  std_logic_vector(2 downto 0);
      s_apb_prdata         : out std_logic_vector(31 downto 0);
      s_apb_pready         : out std_logic;
      s_apb_psel           : in  std_logic;
      s_apb_pslverr        : out std_logic;
      s_apb_pstrb          : in  std_logic_vector(3 downto 0);
      s_apb_pwdata         : in  std_logic_vector(31 downto 0);
      s_apb_pwrite         : in  std_logic
    );
  end component;

  component sync_input is
    generic (
      n_resync: natural := 2  -- n of resync DFFs to reach given MTBF
    );
    port (
      clock: in std_logic;
      d: in std_logic;
      q: out std_logic
    );
  end component;

  component twsi_capt_rxdma is
    generic (
      S2MM_TDATA_WIDTH  : integer  := 32;
      S2MM_COUNT  : integer  := 32
    );
    port (
      aresetn, clock: in std_logic;
      data_in: in std_logic_vector (S2MM_TDATA_WIDTH-1 downto 0);
      time_in: in std_logic_vector (S2MM_TDATA_WIDTH-1 downto 0);
      valid: in std_logic;
      ready: out std_logic;
      xrun_flag: out std_logic;
      dma_trig, dma_reset: in std_logic;
      dma_len: in unsigned (29 downto 0);
      --
      state_mon_o : out std_logic_vector (2 downto 0);
      count_mon_o : out unsigned (29 downto 0);
      --
      S2MM_tvalid : out std_logic;
      S2MM_tready: in std_logic;
      S2MM_tdata: out std_logic_vector (S2MM_TDATA_WIDTH-1 downto 0);
      S2MM_tlast: out std_logic;
      S2MM_tstrb: out std_logic_vector(S2MM_TDATA_WIDTH/8-1 downto 0)
    );
  end component twsi_capt_rxdma;

  component twsi_receiver is
  generic (
    constant data_header : std_logic_vector (7 downto 0) := "11110101";
    rx_iddle_timeout: natural := 100;
    b_out: natural := 4 -- output FIFO width (4 or 8 [bytes])
  );
  port (
    reset: in std_logic;
    clock: in std_logic;
    en: in std_logic;
    out_first: out std_logic;
    out_valid: out std_logic;
    out_data: out std_logic_vector (b_out*8-1 downto 0);
    --
    rx_bit : out std_logic;
    rx_valid : out std_logic;
    --
    rx_di : in std_logic;
    rx_si : in std_logic
  );
  end component;

  signal dma_data, dma_tstamp: std_logic_vector (C_M00_AXIS_TDATA_WIDTH-1 downto 0);
  signal dma_data_valid, dma_trig, dma_rst, dma_xrun: std_logic;
  signal dma_len: unsigned (29 downto 0);
  signal count_mon: unsigned (29 downto 0);
  signal state_mon: std_logic_vector (2 downto 0);
  signal reg_enable : std_logic;
  signal rx_reset : std_logic;
  signal rx_clock : std_logic;

  signal rx_data_valid: std_logic;
  signal rx_data_first: std_logic;
  signal rx_data: std_logic_vector (C_M00_AXIS_TDATA_WIDTH-1 downto 0);
  signal frame_header : std_logic_vector (31 downto 0);
  signal first_recorded : std_logic;

  signal rx_di_sync : std_logic;
  signal rx_si_sync : std_logic;

begin
  rx_clock <= aclk;

  -- Instantiation of APB Interface
  twsi_apb_capt_inst: twsi_capt_apb
  generic map (
    S2MM_TDATA_WIDTH     => C_M00_AXIS_TDATA_WIDTH
  )
  port map (
    aclk                 => aclk,
    arstn                => arstn,

    reg_enable_o         => reg_enable,
    reg_dma_trig_o       => dma_trig,
    reg_dma_reset_o      => dma_rst,
    reg_dma_len_o        => dma_len,
    reg_dma_xrun_i       => dma_xrun,
    reg_state_mon_i      => state_mon,
    reg_count_mon_i      => count_mon,
    reg_la_reset_o       => rx_reset,
    fifo_data_count_i    => fifo_data_count_i,
    fifo_wr_data_count_i => fifo_wr_data_count_i,
    fifo_rd_data_count_i => fifo_rd_data_count_i,
    fifo_reset_n_o       => fifo_reset_n,

    frame_header_i       => frame_header,

    s_apb_paddr          => s_apb_paddr,
    s_apb_penable        => s_apb_penable,
    s_apb_pprot          => s_apb_pprot,
    s_apb_prdata         => s_apb_prdata,
    s_apb_pready         => s_apb_pready,
    s_apb_psel           => s_apb_psel,
    s_apb_pslverr        => s_apb_pslverr,
    s_apb_pstrb          => s_apb_pstrb,
    s_apb_pwdata         => s_apb_pwdata,
    s_apb_pwrite         => s_apb_pwrite
  );

-- Instantiation of Axi Bus Interface M00_AXIS
  twsi_capt_rxdma_inst : twsi_capt_rxdma
  generic map (
    S2MM_TDATA_WIDTH => C_M00_AXIS_TDATA_WIDTH,
    S2MM_COUNT       => C_M00_AXIS_START_COUNT
  )
  port map (
    aresetn     => m00_axis_aresetn,
    clock       => m00_axis_aclk,
    data_in     => dma_data,
    time_in     => dma_tstamp,
    valid       => dma_data_valid,
    -- ready: out std_logic;
    xrun_flag   => dma_xrun,
    dma_trig    => dma_trig,
    dma_reset   => dma_rst,
    dma_len     => dma_len,
    state_mon_o => state_mon,
    count_mon_o => count_mon,
    S2MM_tvalid => m00_axis_tvalid,
    S2MM_tready => m00_axis_tready,
    S2MM_tdata  => m00_axis_tdata,
    S2MM_tstrb  => m00_axis_tstrb,
    S2MM_tlast  => m00_axis_tlast
  );

  -- Add user logic here
twsi_receiver_instance: twsi_receiver
  generic map (
    b_out       => la_b_out
  )
  port map (
    reset       => rx_reset,
    clock       => rx_clock,
    en          => reg_enable,
    out_first   => rx_data_first,
    out_valid   => rx_data_valid,
    out_data    => rx_data,
    --
    rx_bit      => open,
    rx_valid    => open,
    --
    rx_di       => rx_di_sync,
    rx_si       => rx_si_sync
  );

  sync_di: sync_input
    generic map (
      n_resync  => 2
    )
    port map (
      clock => rx_clock,
      d => twsi_di,
      q => rx_di_sync
    );

  sync_si: sync_input
    generic map (
      n_resync  => 2
    )
    port map (
      clock => rx_clock,
      d => twsi_si,
      q => rx_si_sync
    );

  dma_tstamp <= timestamp;

  dma_data_valid <= rx_data_valid and reg_enable and
                    (rx_data_first or first_recorded);
  dma_data <= rx_data;

  first_recorded_process: process(rx_clock)
  begin
    if rising_edge(rx_clock) then
      if rx_reset = '1' or reg_enable = '0' then
        first_recorded <= '0';
      elsif rx_data_first = '1' and rx_data_valid = '1' and
            reg_enable = '1' then
        first_recorded <= '1';
      else
        first_recorded <= first_recorded;
      end if;
    end if;
  end process;

  capture_frame_header: process(rx_clock)
  begin
    if rising_edge(rx_clock) then
      if rx_reset = '1' then
        frame_header <= (others => '0');
      elsif rx_data_first = '1' and rx_data_valid = '1' then
        frame_header <= rx_data(31 downto 0);
      end if;
    end if;
  end process;

  -- User logic ends

end arch_imp;
