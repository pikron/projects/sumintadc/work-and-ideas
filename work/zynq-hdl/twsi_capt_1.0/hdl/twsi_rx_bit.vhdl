library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity twsi_rx_bit is
  port (
    clock : in std_logic;
    reset : in std_logic;
    --
    rx_bit : out std_logic;
    rx_valid : out std_logic;
    --
    rx_di : in std_logic;
    rx_si : in std_logic
    );
end twsi_rx_bit;

architecture rtl of twsi_rx_bit is

  signal rx_bit_st: std_logic;
  signal di_prev: std_logic;
  signal si_prev: std_logic;

begin

  rx_bit_process: process(clock)
  begin
    if rising_edge(clock) then
      if reset = '1' then
        rx_bit_st <= '0';
        rx_valid <= '0';
        di_prev <= '0';
        si_prev <= '0';
      elsif rx_di = di_prev and rx_si = si_prev then
        rx_bit_st <= rx_bit_st;
        di_prev <= di_prev;
        si_prev <= si_prev;
        rx_valid <= '0';
      else
        rx_bit_st <= rx_di;
        di_prev <= rx_di;
        si_prev <= rx_si;
        rx_valid <= '1';
      end if;
    end if;
  end process;

  rx_bit <= rx_bit_st;

end architecture;
