library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.env.stop;

entity twsi_capt_tb is
end twsi_capt_tb;

architecture arch_imp of twsi_capt_tb is

  constant b_out: natural := 4;

  component twsi_frame_serializer is
  generic (
    n_twsi_freq_div : natural := 2;
    n_data_bytes : natural := 8;
    n_data_msgid_bits : natural := 8;
    n_data_len_bits : natural := 8;
    n_data_seq_bits : natural := 8
    );
  port (
    clock : in std_logic;
    reset : in std_logic;
    tx_shift_en : in std_logic;
    tx_rq : in std_logic;
    tx_busy : out std_logic;
    --
    tx_msgid : in std_logic_vector (n_data_msgid_bits-1 downto 0);
    tx_seq : in std_logic_vector (n_data_seq_bits-1 downto 0);
    tx_data : in std_logic_vector ((n_data_bytes*8)-1 downto 0);
    --
    tx_do : out std_logic;
    tx_so : out std_logic
    );
  end component;

  component twsi_receiver is
  generic (
    constant data_header : std_logic_vector (7 downto 0) := "11110101";
    rx_iddle_timeout: natural := 100;
    b_out: natural := 4 -- output FIFO width (4 or 8 [bytes])
  );
  port (
    reset: in std_logic;
    clock: in std_logic;
    en: in std_logic;
    out_first: out std_logic;
    out_valid: out std_logic;
    out_data: out std_logic_vector (b_out*8-1 downto 0);
    --
    rx_bit : out std_logic;
    rx_valid : out std_logic;
    --
    rx_di : in std_logic;
    rx_si : in std_logic
  );
  end component;

  constant clk_period : time := 10 ns;

  signal clock : std_logic := '0';
  signal reset : std_logic := '1';

  signal twsi_d : std_logic;
  signal twsi_s : std_logic;

  constant data_bytes : natural := 8;

  signal tx_data : std_logic_vector ((data_bytes*8)-1 downto 0);

  signal tx_rq : std_logic;
  signal tx_busy : std_logic;

  signal delay_cnt: integer;

  -- TWSI Receiver

  signal rx_bit : std_logic;
  signal rx_valid : std_logic;

  signal out_first: std_logic;
  signal out_valid: std_logic;
  signal out_data: std_logic_vector (b_out*8-1 downto 0);

  signal out_data_strobed: std_logic_vector (b_out*8-1 downto 0);

begin

  -- Instantiate the Unit Under Test (UUT)
  twsi_tx_gen: twsi_frame_serializer
  generic map (
    n_twsi_freq_div => 3,
    n_data_bytes   => data_bytes
  )
  port map (
    reset => reset,
    clock => clock,
    tx_shift_en => '1',
    tx_rq => tx_rq,
    tx_busy => tx_busy,
    --
    tx_msgid => x"10",
    tx_seq => x"AA",
    tx_data => tx_data,
    --
    tx_do => twsi_d,
    tx_so => twsi_s
  );


  twsi_receiver_uut : twsi_receiver
  generic map (
    b_out => b_out
  )
  port map (
    reset => reset,
    clock => clock,
    en => '1',
    out_first => out_first,
    out_valid => out_valid,
    out_data => out_data,
    --
    rx_bit => rx_bit,
    rx_valid => rx_valid,
    --
    rx_di => twsi_d,
    rx_si => twsi_s
  );

  clk_process :process
  begin
    clock <= '1';
    wait for clk_period/2;
    clock <= '0';
    wait for clk_period/2;
  end process;

  strobe_data_process : process(clock)
  begin
    if rising_edge(clock) then
      if reset = '1' then
        out_data_strobed <= (others => '0');
      elsif out_valid = '1' then
        out_data_strobed <= out_data;
      end if;
    end if;
  end process;

  test_process :process
  begin
    tx_rq <= '0';
    reset <= '1';
    wait until rising_edge (clock);
    reset <= '0';
    wait until rising_edge (clock);
    wait until rising_edge (clock);
    tx_data <= x"12345678aabbccdd";
    tx_rq <= '1';
    delay_cnt <= (data_bytes + 4) * 8 * 3 + 4;
    wait until rising_edge (clock);
    tx_rq <= '0';
    while (delay_cnt > 0) loop
      delay_cnt <= delay_cnt - 1;
      wait until rising_edge (clock);
    end loop;
    wait until rising_edge (clock);
    tx_data <= x"abcdef1122334455";
    tx_rq <= '1';
    delay_cnt <= (data_bytes + 4) * 8 * 3 + 4;
    wait until rising_edge (clock);
    tx_rq <= '0';
    while (delay_cnt > 0) loop
      delay_cnt <= delay_cnt - 1;
      wait until rising_edge (clock);
    end loop;
    wait until rising_edge (clock);
    report "Calling 'stop'";
    stop;
  end process;

end arch_imp;
